
package champion;

import arenaobservable.ArenaObserver;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import arena.Square;
import exception.AlreadyDeadException;
import injector.AppInjector;
import move.Move;
import util.Util;
import weapon.Weapon;

import java.util.List;
import java.util.Random;

public class Champion implements Comparable<Champion> {

    private int playerId;
    private int teamId;
    private int id;
    private String name;
    private Square square = new Square(-1, -1);
    private List<ChampionClass> championClasses = new ArrayList<>();
    private List<Weapon> weapons = new ArrayList<>();
    private int cost;
    private Attributes attributes;
    private Attributes attackAttributes;
    private String ability;
    private int maxWeapons = 3;
    private AbilityType abilityType;
    private JsonObject originalAttributes;


    private transient List<Move> moves = new ArrayList<>();
    private transient ArenaObserver arenaObserver = AppInjector.getArenaObserver();

    public Champion(JsonObject jsonObject) {
        initializeWith(jsonObject);
    }

    private void initializeWith(JsonObject jsonObject) {
        originalAttributes = jsonObject;
        attributes = new Attributes();
        attackAttributes = new Attributes();
        setId(jsonObject.get("id").getAsInt());
        setName(jsonObject.get("Champion").getAsString());
        getChampionClasses().clear();
        getChampionClasses().add(ChampionClass.valueOf(jsonObject.get("Class1").getAsString()));
        if (!jsonObject.get("Class2").getAsString().equals("-")) {
            getChampionClasses().add(ChampionClass.valueOf(jsonObject.get("Class2").getAsString()));
        }
        if (!jsonObject.get("Class3").getAsString().equals("-")) {
            getChampionClasses().add(ChampionClass.valueOf(jsonObject.get("Class3").getAsString()));
        }
        setCost(jsonObject.get("GoldCost").getAsInt());
        attributes.setHealthCapacity(jsonObject.get("Health").getAsInt());
        attributes.setHealth(jsonObject.get("Health").getAsInt());
        attributes.setArmor(Integer.parseInt(jsonObject.get("Armor").getAsString().replace("%", "")));
        attributes.setMagicResist(Integer.parseInt(jsonObject.get("Magic resist").getAsString().replace("%", "")));
        attributes.setVisionRange(jsonObject.get("Vision range").getAsInt());
        attributes.setAttackRange(jsonObject.get("Attack range").getAsInt());
        attributes.setAttackDamage(jsonObject.get("AttackDamage").getAsInt());
        attributes.setMovementSpeed(jsonObject.get("Movement speed").getAsInt());
        attributes.setCriticalStrikeChance(Integer.parseInt(jsonObject.get("Critical strike chance").getAsString().replace("%", "")));
        attributes.setCriticalStrikeDamage(Integer.parseInt(jsonObject.get("Critical strike damage").getAsString().replace("%", "")));
        attributes.setManaStart(jsonObject.get("Mana Start").getAsInt());
        attributes.setManaCost(jsonObject.get("Mana Cost").getAsDouble());
        attributes.setMana(attributes.getManaStart());
        ability = jsonObject.get("Ability").getAsString();
        if (jsonObject.get("AbilityType").getAsString().equals("AOE")) {
            setAbilityType(AbilityType.AOE);
        } else {
            setAbilityType(AbilityType.TARGET);
        }
    }

    @Override
    public Champion clone() {
        Champion champion = new Champion(getOriginalAttributes());
        champion.setOriginalAttributes(originalAttributes);
        return champion;
    }

    public AbilityType getAbilityType() {
        return abilityType;
    }

    public void setAbilityType(AbilityType abilityType) {
        this.abilityType = abilityType;
    }

    public JsonObject getOriginalAttributes() {
        return originalAttributes;
    }

    public void setOriginalAttributes(JsonObject originalAttributes) {
        this.originalAttributes = originalAttributes;
    }

    public Square getSquare() {
        return square;
    }

    public void setSquare(Square square) {
        if (arenaObserver == null)
            arenaObserver = AppInjector.getArenaObserver();
        arenaObserver.notify(this, square);
//        square.setX(Math.max(0, Math.min(AppInjector.getConfig().getArenaSize() - 1, square.getX())));
//        square.setY(Math.max(0, Math.min(AppInjector.getConfig().getArenaSize() - 1, square.getY())));
        this.square = square;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getId() {
        return id;
    }

    public void setArenaObserver(ArenaObserver arenaObserver) {
        if (this.arenaObserver == null) {
            this.arenaObserver = arenaObserver;
            this.arenaObserver.notify(this, square);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChampionClass> getChampionClasses() {
        return championClasses;
    }

    public void setChampionClasses(List<ChampionClass> championClasses) {
        this.championClasses = championClasses;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public List<Move> getMoves() {
        if (moves == null) moves = new ArrayList<>();
        return moves;
    }

    public void setMoves(List<Move> moves) {
        this.moves = moves;
    }

    public void addMove(Move move) {
        if (moves == null) moves = new ArrayList<>();
        moves.add(move);
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public Attributes getAttackAttributes() {
        return attackAttributes;
    }

    public void addToAttackAttributes(Attributes attributes) {
        this.attackAttributes.merge(attributes);
    }

    public void addToAttributes(Attributes attributes) {
        this.attributes.merge(attributes);
    }

    public void setAttackAttributes(Attributes attackAttributes) {
        this.attackAttributes = attackAttributes;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void acceptDamage(Attributes attributes) {
        if (this.attributes.isDead()) {
            try {
                throw new AlreadyDeadException();
            } catch (AlreadyDeadException e) {
                System.err.println(e.getMessage());
                return;
            }
        }
        int defensive = (attributes.abilityType == AbilityType.basicAttack ? this.getAttributes().getArmor() : this.getAttributes().getMagicResist());
        defensive += this.attributes.getLessDamageChance();
        defensive = Math.min(100, defensive);
        if (this.getAttributes().getImmunityDuration() > 0) return;
        if (attributes.abilityType == AbilityType.basicAttack && this.getAttributes().getImmunityBasicAttackDuration() > 0) return;
        Random random = new Random();
        if (random.nextInt(100) <= this.getAttributes().getMissTheAttack() && attributes.abilityType == AbilityType.basicAttack)
            return;
        if (attributes.isTrueDamage()) {
            this.attributes.merge(attributes);
            return;
        }
        attributes.setHealth(attributes.getHealth() - (defensive * attributes.getHealth()) / 100);
        this.attributes.merge(attributes);
        if (this.attributes.getHealth() <= 0) {
            if (arenaObserver == null)
                arenaObserver = AppInjector.getArenaObserver();
            arenaObserver.notifyDead(this);
            this.attributes.setDead(true);
            this.attributes.setHealth(0);
            System.out.println(getName() + " dead");
        } else if (this.attributes.getHealth() > this.attributes.getHealthCapacity()) {
            this.attributes.setHealth(this.attributes.getHealthCapacity());
        }
    }

    public boolean addWeapon(Weapon weapon) {
        if (this.weapons.size() < maxWeapons && Util.isSameClass(this, weapon.getChampionClass())) {
            this.weapons.add(weapon);
            addToAttributes(weapon.getWeaponBuff());
            return true;
        }
        return false;
    }

    public Champion reset() {
        Champion champion = new Champion(originalAttributes);

        // this values should remain the same as old version
//        champion.setPlayerId(playerId);
//        champion.setTeamId(teamId);
//        champion.setMoves(moves);
        champion.getAttributes().setHealth(attributes.getHealth());
        champion.getAttributes().setMana(attributes.getMana());
        champion.getAttributes().setStunDuration(attributes.getStunDuration());
        champion.getAttributes().setImmunityDuration(attributes.getImmunityDuration());
        champion.getAttributes().setImmunityBasicAttackDuration(attributes.getImmunityBasicAttackDuration());
        champion.getAttributes().setLevel(attributes.getLevel());


        initializeWith(originalAttributes);
        getAttributes().setHealth(champion.getAttributes().getHealth());
        getAttributes().setMana(champion.getAttributes().getMana());
        getAttributes().setStunDuration(champion.getAttributes().getStunDuration());
        getAttributes().setImmunityDuration(champion.getAttributes().getImmunityDuration());
        getAttributes().setImmunityBasicAttackDuration(champion.getAttributes().getImmunityBasicAttackDuration());
        getAttributes().setLevel(champion.getAttributes().getLevel());


        return this;
    }


    @Override
    public int compareTo(Champion o) {
        if (o == null)
            return 1;

        if (attributes.getLevel() > o.getAttributes().getLevel())
            return 1;
        else if (attributes.getLevel() < o.getAttributes().getLevel())
            return 2;
        else if (attributes.getAttackDamage() > o.getAttributes().getAttackDamage())
            return 1;
        else if (attributes.getAttackDamage() < o.getAttributes().getAttackDamage())
            return 2;
        else if (attributes.getCriticalStrikeChance() > o.getAttributes().getCriticalStrikeChance())
            return 1;
        else if (attributes.getCriticalStrikeChance() < o.getAttributes().getCriticalStrikeChance())
            return 2;
        else if (attributes.getHealthCapacity() > o.getAttributes().getHealthCapacity())
            return 1;
        else if (attributes.getHealthCapacity() < o.getAttributes().getHealthCapacity())
            return 2;
        else
            return (int) (Math.random() * 2) + 1;

    }

    @Override
    public String toString() {
        return "Name : " + getName() + ", "
                + "Player id : " + getPlayerId() + ", "
                + "Health : " + getAttributes().getHealth() + "/" + getAttributes().getHealthCapacity() + ", "
                + "Position : " + "(" + getSquare().getX() + ", " + getSquare().getY() + ")" + ", "
                + "Mana :" + getAttributes().getMana() + ", "
                + "Attack Range : " + getAttributes().getVisionRange() + ", "
                + "Hash : " + Integer.toHexString(hashCode());
    }
}