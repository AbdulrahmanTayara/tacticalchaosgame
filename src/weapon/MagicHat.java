package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class MagicHat extends Weapon{

    public MagicHat(){
        super(WeaponType.MAGIC_HAT.name(), "Magic Hat");
        super.setChampionClass(ChampionClass.Sorcerer);
    }
    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setAbilityPower(20);
        return attributes;
    }
}
