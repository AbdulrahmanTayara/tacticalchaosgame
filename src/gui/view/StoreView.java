package gui.view;

import champion.Champion;
import gui.component.DetailsTooltip;
import gui.GameFrame;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

import java.util.List;

public class StoreView {

    private List<Champion> storeChampions;
    private HBox hBox;

    public StoreView(List<Champion> storeChampions) {
        this.storeChampions = storeChampions;
        this.hBox = new HBox();
        this.hBox.getStyleClass().add("theme");
        this.prepareStore();
    }

    private void prepareStore() {
        this.hBox.getChildren().clear();
        int i = 0;
        for (Champion champion : storeChampions) {
            VBox vBox = new VBox();
            Label name = new Label(champion.getName());
            Label price = new Label(Integer.toString(champion.getCost()));

            HBox buttons = new HBox();
            buttons.setAlignment(Pos.CENTER);
            Button details = new Button("Details");
            DetailsTooltip detailsTooltip = new DetailsTooltip(champion);
            details.setOnMousePressed(actionEvent -> {
                detailsTooltip.toggleTooltip(details, actionEvent.getScreenX(), actionEvent.getScreenY());
            });
            Button buy = new Button("Buy");
            int j = i;
            buy.setOnAction(actionEvent -> {
                GameFrame.getInstance().pickChampionToBuy(champion, j);
                prepareStore();
            });
            Label isRecommended = new Label();
            isRecommended.setTextAlignment(TextAlignment.CENTER);
            isRecommended.setMinSize(0,0);
            isRecommended.setStyle("-fx-font-weight: bold; -fx-text-fill: lightgreen;");
            if(GameFrame.getInstance().getPresenter().getPlayer().isRecommended(champion) == 1) {
                isRecommended.setText("✶ Recommended ✶");
                Tooltip tooltip = new Tooltip("Champion can be upgraded to the next level!");
                tooltip.setShowDelay(Duration.ZERO);
                tooltip.setHideDelay(Duration.ZERO);
                isRecommended.setTooltip(tooltip);
            } else if(GameFrame.getInstance().getPresenter().getPlayer().isRecommended(champion) == 2) {
                isRecommended.setText("✶ Recommended ✶");
                Tooltip tooltip = new Tooltip("Champions on the arena with the same class!");
                tooltip.setShowDelay(Duration.ZERO);
                tooltip.setHideDelay(Duration.ZERO);
                isRecommended.setTooltip(tooltip);
            }
            i++;
            buttons.getChildren().addAll(buy, details);
            vBox.getChildren().addAll(name, isRecommended, price, buttons);
            HBox.setHgrow(vBox, Priority.ALWAYS);
            vBox.setMaxWidth(Double.MAX_VALUE);
            vBox.setAlignment(Pos.CENTER);

            HBox.setMargin(vBox, new Insets(10));
            this.hBox.getChildren().add(vBox);
        }
    }

    public Node getStoreView() {
        return this.hBox;
    }


}
