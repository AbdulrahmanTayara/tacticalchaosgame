package player;

import java.util.ArrayList;
import java.util.List;

public class Team {
    //Autoincrement id
    private static int id = 1;

    private int teamId;
    private List<Player> playersInTeam = new ArrayList<>();

    public Team(){
        teamId = id++;
    }
    public List<Player> getPlayersInTeam() {
        return playersInTeam;
    }

    public int getId() {
        return teamId;
    }

    public void addPlayersToTeam(Player player) {
        player.setTeamId(getId());
        this.playersInTeam.add(player);
    }

    public void checkPlayers() {
        getPlayersInTeam().removeIf(player -> player.getChampionsInArena().size() == 0);
    }

}
