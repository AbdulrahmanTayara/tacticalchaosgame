package player;


import arena.Square;
import champion.Attributes;
import champion.Champion;
import com.google.gson.annotations.SerializedName;
import game.IGamePresenter;
import injector.AppInjector;
import util.Util;

import java.util.ArrayList;
import java.util.List;

public abstract class Player implements IView {

    protected transient IGamePresenter presenter;

    private static int id = 1;
    private int playerId;
    private int teamId;
    private int coins = 0;
    private List<Champion> championsInArena = new ArrayList<>();
    private List<Champion> championsInBench = new ArrayList<>();
    private int numberOfPermissibleSwaps;
    private int numberOfChampionInArena = AppInjector.getConfig().getPlayerChampionsInArena();

    @SerializedName("playerType")
    private String playerType;

    public Player(String playerType) {
        playerId = id++;
        //presenter = AppInjector.getGame();
        this.playerType = playerType;
    }

    public void setPresenter(IGamePresenter gamePresenter) {
        presenter = gamePresenter;
    }

    public int getNumberOfPermissibleSwaps() {
        return numberOfPermissibleSwaps;
    }

    public void setNumberOfPermissibleSwaps(int numberOfPermissibleSwaps) {
        this.numberOfPermissibleSwaps = numberOfPermissibleSwaps;
    }

    public int getCoins() {
        return coins;
    }

    public int getId() {
        return playerId;
    }

    public void addChampionToBench(Champion champion) {
        champion.setSquare(new Square(-1, -1));
        championsInBench.add(champion);
    }

    public void removeChampionFromBench(Champion champion) {
        try {
            championsInBench.remove(champion);
        } catch (NullPointerException e) {
        }
    }

    public void addChampionToArena(Champion champion) {
        championsInArena.add(champion);
    }

    public void removeChampionFromArena(Champion champion) {
        try {
            champion.setSquare(new Square(-1, -1));
            championsInArena.remove(champion);
        } catch (NullPointerException e) {
        }
    }

    public void swapFromBenchToArena(Champion champion) {
        removeChampionFromBench(champion);
        addChampionToArena(champion);
    }

    public void swapFromArenaToBench(Champion champion) {
        removeChampionFromArena(champion);
        addChampionToBench(champion);
    }

    public List<Champion> getChampionsInArena() {
        return championsInArena;
    }

    public List<Champion> getChampionsInBench() {
        return championsInBench;
    }

    public boolean canBuy(int cost) {
        return coins >= cost && getChampionsInBench().size() < AppInjector.getConfig().getPlayerChampionsInBench();
    }

    public void buyChampion(Champion champion) {
        if (champion == null) return;
        champion.setPlayerId(getId());
        champion.setTeamId(getTeamId());
        if (!checkLevelOfChampion(champion)) {
            addChampionToBench(champion);
        }
        coins -= champion.getCost();
    }

    public int isRecommended(Champion champion) {
        if (canUpgrade(champion, champion.getAttributes().getLevel()))
            return 1;
        else if (hasClassMate(champion))
            return 2;
        return  0;
    }

    private boolean canUpgrade(Champion champion, int level) {
        int counter = 0;
        for (Champion champion1 : championsInArena) {
            if (champion.getId() == champion1.getId() && champion1.getAttributes().getLevel() == level) counter++;
        }
        for (Champion champion1 : championsInBench) {
            if (champion.getId() == champion1.getId() && champion1.getAttributes().getLevel() == level) counter++;
        }
        return counter == 2;
    }

    private boolean hasClassMate(Champion champion) {
        for (Champion champion1 : getChampionsInArena()) {
            if (Util.isSameClass(champion1, champion))
                return true;
        }
        return false;
    }

    private boolean checkLevelOfChampion(Champion champion) {
        boolean added = false;
        for (int level = 1; level <= AppInjector.getConfig().getChampionMaxLevel(); ++level) {
            if (canUpgrade(champion, level)) {
                int finalLevel = level;
                championsInBench.removeIf(champion1 -> {
                    return champion1.getId() == champion.getId() && champion1.getAttributes().getLevel() == finalLevel;
                });

                championsInArena.removeIf(champion1 -> {
                    return champion1.getId() == champion.getId() && champion1.getAttributes().getLevel() == finalLevel;
                });

                increaseAttributes(champion, level);
                addChampionToBench(champion);
                added = true;
            }
        }
        return added;
    }

    private void increaseAttributes(Champion champion, int level) {
        Attributes attributes = champion.getAttributes();
        switch (level) {
            case 1:
                attributes.setAttackDamage(Util.increaseByPercent(attributes.getAttackDamage(), 10));
                attributes.setHealthCapacity(Util.increaseByPercent(attributes.getHealthCapacity(), 20));
                attributes.setHealth(attributes.getHealthCapacity());
                attributes.setArmor(Util.increaseByPercent(attributes.getArmor(), 20));
                attributes.setMagicResist(Util.increaseByPercent(attributes.getMagicResist(), 20));
                attributes.setLevel(2);
                break;
            case 2:
                attributes.setAttackDamage(Util.increaseByPercent(attributes.getAttackDamage(), 15));
                attributes.setHealthCapacity(Util.increaseByPercent(attributes.getHealthCapacity(), 25));
                attributes.setHealth(attributes.getHealthCapacity());
                attributes.setArmor(Util.increaseByPercent(attributes.getArmor(), 25));
                attributes.setMagicResist(Util.increaseByPercent(attributes.getMagicResist(), 25));
                attributes.setLevel(3);
                break;
        }
    }

    public void increaseGold(int value) {
        coins += value;
    }


    public void sellChampion(Champion champion) {
        try {
            championsInArena.remove(champion);
            championsInBench.remove(champion);
            coins += champion.getCost();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void checkChampions() {
        getChampionsInArena().removeIf(champion -> {
                    if (champion.getAttributes().getHealth() == 0) {
                        numberOfChampionInArena--;
                        return true;
                    }
                    return false;
                }
        );
    }

    public int getNumberOfChampionInArena() {
        return numberOfChampionInArena;
    }

    public void increaseNumberOfChampionInArena(int numberOfChampionInArena) {
        this.numberOfChampionInArena += numberOfChampionInArena;
    }

    public void resetChampions() {
        // new champions
        List<Champion> arenaChampions = new ArrayList<>();
        List<Champion> benchChampions = new ArrayList<>();
        getChampionsInArena().forEach(champion -> arenaChampions.add(champion.reset()));
        getChampionsInBench().forEach(champion -> benchChampions.add(champion.reset()));
        // reset

        championsInArena.clear();
        championsInArena.addAll(arenaChampions);
        championsInBench.clear();
        championsInBench.addAll(benchChampions);
    }

    public abstract void printPlayerChampions(List<Champion> champions);

    public abstract String getColor();

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }
}
