package move;

import champion.Champion;
import player.Player;

public class BuyMove extends Move{
    Player player;
    Champion champion;
    BuyMove(Player player , Champion champion){
        this.player = player;
        this.champion = champion;
    }
    @Override
    public void performMove() {
        int gold = player.getCoins();
        int cost = champion.getCost();
        if(gold >= cost)
        player.addChampionToBench(champion);
    }
}
