package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveSwainAbilityMove extends Move{
    Champion attacker;
    private static int abilityDamage = 50;
    private static double radius = 15;

    public ActiveSwainAbilityMove(Champion attacker) {
        this.attacker = attacker;
        numberOfRound +=3;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getEnemiesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
        Attributes attributes = new Attributes();
        attributes.setHealth(abilityDamage * list.size());
        attacker.addToAttributes(attributes);
    }
}
