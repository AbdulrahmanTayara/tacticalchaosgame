package move;

import champion.Attributes;
import champion.Champion;

public class ActiveVarusAbilityMove extends Move{
    Champion attacker;

    public ActiveVarusAbilityMove(Champion attacker) {
        this.attacker = attacker;
        numberOfRound += 4;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int criticalStrikeChance = attacker.getAttributes().getCriticalStrikeChance();
        attributes.setCriticalStrikeChance(criticalStrikeChance * 400/100);
        attacker.addToAttributes(attributes);
    }
}
