package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class NightShift extends Weapon{

    public NightShift() {
        super(WeaponType.NIGHT_SHIFT.name(), "Night Shift");
        super.setChampionClass(ChampionClass.Assassin);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setAttackDamage(20);
        return attributes;
    }
}
