package arena;

import arenaobservable.ArenaObserver;
import champion.Champion;
import move.Direction;
import util.Util;
import weapon.Weapon;

import java.util.*;

public class Arena implements ArenaObserver {
    private ArenaSquare[][] squares;
    private int arenaSize;
    private int numberOfWeapons, percent;
    private boolean isInitialized = false;

    public Arena(int arenaSize) {
        squares = new ArenaSquare[arenaSize][arenaSize];
        this.arenaSize = arenaSize;
        percent = (int) (arenaSize * arenaSize * 0.03);
        numberOfWeapons = (int) (arenaSize * arenaSize * 0.05);
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                ArenaSquare square = new ArenaSquare();
                squares[i][j] =  square;
            }
        }
        //setSquaresType();
    }

    public void setSquaresType() {
        setInitialized(true);
        List<ArenaSquare> squaresTemp = new ArrayList<>();

        Random random = new Random();
        for (ArenaSquare[] square : squares) {
            squaresTemp.addAll(Arrays.asList(square).subList(0, squares.length));
        }

        Collections.shuffle(squaresTemp);
        for (int i = 0; i < percent; i++) {
            for (int j = 0; j < 3; j++) {
                squaresTemp.get(0).setSquareType(SquareType.values()[j]);
                if(random.nextBoolean() && numberOfWeapons > 0) {
                    squaresTemp.get(0).addWeapon(Weapon.getRandomWeapon());
                    numberOfWeapons--;
                }
                squaresTemp.remove(0);
            }
        }
        while (numberOfWeapons > 0){
            squaresTemp.get(0).addWeapon(Weapon.getRandomWeapon());
            squaresTemp.remove(0);
            numberOfWeapons--;
        }

    }

    private void checkInRange(Square square) {
        square.setX(Math.max(0 , Math.min(square.getX() , arenaSize - 1)));
        square.setY(Math.max(0 , Math.min(square.getY() , arenaSize - 1)));
    }

    @Override
    public void notify(Champion champion, Square newSquare) {
        int newX = newSquare.getX();
        int newY = newSquare.getY();
        removeChampion(champion);
        if (newX == -1 || newY == -1) {
            return;
        }
        checkInRange(newSquare);
        Direction direction = Util.getDirection(champion.getSquare(), newSquare);
        Square targetSquare = goTo(champion, champion.getSquare(), newSquare, direction);
        newSquare.setX(targetSquare.getX());
        newSquare.setY(targetSquare.getY());
        checkInRange(newSquare);
        squares[newSquare.getX()][newSquare.getY()].addChampion(champion);
        if (squares[newSquare.getX()][newSquare.getY()].getSquareType() == SquareType.Grass) {
            champion.getAttributes().setVisible(false);
        } else
            champion.getAttributes().setVisible(true);
    }

    @Override
    public void notifyDead(Champion champion) {
        int x = champion.getSquare().getX();
        int y = champion.getSquare().getY();
        ArenaSquare square = squares[x][y];
        square.getWeapons().addAll(champion.getWeapons());
        removeChampion(champion);
    }

    private void removeChampion(Champion champion) {
        try {
            squares[champion.getSquare().getX()][champion.getSquare().getY()].removeChampion(champion);
        } catch (Exception ignored) {}
    }

    private Square goTo(Champion champion, Square source, Square destination, Direction direction) {
        if (source.getX() == -1 || source.getY() == -1) {
           source.setX(destination.getX());
           source.setY(destination.getY());
        }
        ArenaSquare square = squares[source.getX()][source.getY()];
        // fixme : if the terrain in the corner
        if (square.getSquareType() == SquareType.Terrain)
            return (new Square(source.getX() - direction.getX(), source.getY() - direction.getY()));
        for(int i=0;i<square.getWeapons().size();i++){
            Weapon weapon = square.getWeapons().get(i);
            if(champion.addWeapon(weapon)){
                square.getWeapons().remove(weapon);
                --i;
            }
        }
        if (source.equals(destination))
            return source;
        if (square.getSquareType() == SquareType.Water) {
            int targetX = (source.getX() + destination.getX()) / 2;
            int targetY = (source.getY() + destination.getY()) / 2;
            Direction newDirection = Util.getDirection(source, new Square(targetX, targetY));
            if (direction != newDirection)
                return source.clone();
            return goTo(champion, source.move(direction), new Square(targetX, targetY), direction);
        } else {
            return goTo(champion, source.move(direction), destination, direction);
        }
    }

    public ArenaSquare getArenaSquare(int x, int y) {
        return squares[x][y];
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setInitialized(boolean initialized) {
        isInitialized = initialized;
    }

//    public void printAllChamps() {
//        System.out.println("Champs in Arena : ");
//        for (int i = 0; i < arenaSize; i++) {
//            for (int j = 0; j < arenaSize; j++) {
//                ArenaSquare arenaSquare = squares[i][j];
//                arenaSquare.getChampions().forEach(System.out::println);
//            }
//        }
//    }
}
