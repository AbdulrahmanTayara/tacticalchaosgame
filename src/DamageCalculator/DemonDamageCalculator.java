package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class DemonDamageCalculator {
    public static void Calculate(List<Champion> demons) {
        Attributes attributes = new Attributes();
        if (demons.size() >= 2 && demons.size() < 4) {
            attributes.setMana(-20);
            for(Champion champion : demons)
                champion.addToAttackAttributes(attributes);
        } else if (demons.size() >= 4) {
            attributes.setMana(-40);
            for(Champion champion : demons)
                champion.addToAttackAttributes(attributes);
        }
    }
}
