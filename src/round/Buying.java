package round;


import injector.AppInjector;
import player.Player;
import store.Store;


public class Buying extends Round {

    public Buying() {
        super(RoundType.BUYING.name());
    }

    @Override
    public void start(Player player) {
        player.setNumberOfPermissibleSwaps(Integer.MAX_VALUE);
        player.increaseGold(AppInjector.getConfig().getNumberOfCoinsEachRound());  // give the player (2 coins)
        player.startBuyingRound();
    }

}
