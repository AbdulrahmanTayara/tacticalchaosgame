package player;

public interface IView {
    void startBuyingRound();
    void startPlanningRound();
    void gameFinished(Team winner);
    void gameFinishedAsDraw();
}
