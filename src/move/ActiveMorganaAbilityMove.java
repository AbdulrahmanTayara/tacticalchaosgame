package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveMorganaAbilityMove extends Move{
    private static int abilityDamage = 100;
    private static double radius = 25;
    Champion attacker;

    public ActiveMorganaAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List <Champion> list = Util.getEnemiesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
        Attributes attributes = new Attributes();
        attributes.setMana(2 * list.size());
        attacker.addToAttributes(attributes);
    }
}
