package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class KnightDamageCalculator {
    public static void Calculate(List<Champion> allArenaChampions) {
        Attributes attributes = new Attributes();
        if (allArenaChampions.size() >= 2 && allArenaChampions.size() < 4) {
            attributes.setLessDamageChance(5);
            for (Champion champion : allArenaChampions)
                champion.addToAttributes(attributes);
        } else if (allArenaChampions.size() >= 4 && allArenaChampions.size() < 6) {
            attributes.setLessDamageChance(8);
            for (Champion champion : allArenaChampions)
                champion.addToAttributes(attributes);
        } else if (allArenaChampions.size() >= 6) {
            attributes.setLessDamageChance(12);
            for (Champion champion : allArenaChampions)
                champion.addToAttributes(attributes);
        }
    }
}
