package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveGnarAbilityMove extends Move{

    Champion attacker;
    private static int abilityDamage = 100;
    public ActiveGnarAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List <Champion> list = Util.getEnemiesBehindChampion(attacker.getSquare() , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
