package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;
import util.Util;

import java.util.List;

public class ActiveYasuoAbilityMove extends Move{
    Champion attacker;
    private static int abilityDamage = 150;

    public ActiveYasuoAbilityMove(Champion attacker){
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getEnemiesBehindChampion(attacker.getSquare() ,  attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setAbilityType(AbilityType.ability);
            attributes.setStunDuration(2);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
