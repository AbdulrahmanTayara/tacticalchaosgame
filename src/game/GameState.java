package game;

public enum GameState {
    WINNER,
    DRAW,
    RUNNING
}
