package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class GlacialDamageCalculator {
    public static void Calculate(List<Champion> glacials) {
        Attributes attributes=new Attributes();
        attributes.setStunDuration(1);
        for(Champion champion : glacials)
            champion.addToAttackAttributes(attributes);
    }
}
