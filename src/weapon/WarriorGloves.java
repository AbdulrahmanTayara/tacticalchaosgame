package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class WarriorGloves extends Weapon {

    public WarriorGloves() {
        super(WeaponType.WARRIOR_GLOVES.name(), "Warrior Gloves");
        super.setChampionClass(ChampionClass.BladeMaster);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setAttackDamage(10);
        return attributes;
    }
}
