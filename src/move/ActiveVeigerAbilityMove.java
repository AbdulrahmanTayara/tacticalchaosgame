package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;

public class ActiveVeigerAbilityMove extends Move{
    Champion attacker , target;
    public static int inf = Integer.MAX_VALUE;

    public ActiveVeigerAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setHealth(-inf);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
