package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class UniverseCore extends Weapon{

    public UniverseCore() {
        super(WeaponType.UNIVERSE_CORE.name(), "Universe Core");
        super.setChampionClass(ChampionClass.Elementalist);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setMagicResist(20);
        return attributes;
    }
}
