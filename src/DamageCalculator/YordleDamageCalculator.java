package DamageCalculator;

import champion.Attributes;
import champion.Champion;
import player.Player;

import java.util.List;

public class YordleDamageCalculator {
    public static void Calculate(List<Champion> yordles) {
        Attributes attributes = new Attributes();
        if (yordles.size() >= 2 && yordles.size() < 4)
            for (Champion champion : yordles) {
                attributes.setMissTheAttack(15);
                champion.addToAttributes(attributes);
            }

        else if (yordles.size() >= 4 && yordles.size() < 6)
            for (Champion champion : yordles) {
                attributes.setMissTheAttack(30);
                champion.addToAttributes(attributes);
            }

        else if (yordles.size() >= 6)
            for (Champion champion : yordles) {
                attributes.setMissTheAttack(50);
                champion.addToAttributes(attributes);
            }
    }
}
