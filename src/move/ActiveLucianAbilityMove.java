package move;

import champion.Attributes;
import champion.Champion;

public class ActiveLucianAbilityMove extends Move{
    Champion attacker;

    public ActiveLucianAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setAttackRange(10);
        attributes.setMovementSpeed(10);
        attacker.addToAttributes(attributes);
    }
}
