package store;

import champion.Champion;
import injector.AppInjector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RandomStore extends Store {

    public RandomStore() {
        super();
    }

    @Override
    public void generateChampions() {
        List<Champion> availableChampions = this.championsList.stream().filter(champion -> {
            return this.championsCount[champion.getId()] > 0;
        }).collect(Collectors.toList());
        Collections.shuffle(availableChampions);
        this.roundChampionsList = new ArrayList<>();
        int i = 0;
        for (Champion champion:availableChampions) {
            if(i++ < AppInjector.getConfig().getRandomStoreListSize()) this.roundChampionsList.add(champion);
            else break;
        }
    }

}
