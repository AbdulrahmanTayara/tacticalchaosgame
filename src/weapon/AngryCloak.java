package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class AngryCloak extends Weapon {

    public AngryCloak() {
        super(WeaponType.ANGRY_CLOAK.name(), "Angry Cloak");
        super.setChampionClass(ChampionClass.Yordle);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setCriticalStrikeChance(10);
        return attributes;
    }
}
