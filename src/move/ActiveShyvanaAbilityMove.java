package move;

import champion.Attributes;
import champion.Champion;

public class ActiveShyvanaAbilityMove extends Move{
    Champion attacker;

    public ActiveShyvanaAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        int currHealth = attacker.getAttributes().getHealth();
        Attributes attributes = new Attributes();
        attributes.setHealth(currHealth * 10/100);
        attributes.setAttackRange(5);
        attacker.addToAttributes(attributes);
    }
}
