package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;
import util.Util;

import java.util.List;

public class ActivePoppyAbilityMove extends Move{
       Champion attacker;

    public ActivePoppyAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getEnemiesInALine(attacker.getSquare() , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setStunDuration(2);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
