package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

public class ActiveKhazixAbilityMove extends Move{
    private static int inf = Integer.MAX_VALUE;
    Champion attacker , target;


    public ActiveKhazixAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        double radius = Util.getNearestAllieRadius(target.getSquare() , target.getPlayerId() , target.getId());
        if(radius <= 20.0)  attributes.setHealth(-inf);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
