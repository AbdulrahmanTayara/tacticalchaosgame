package game;

import arena.Arena;
import player.Team;
import round.Round;
import util.Config;

import java.util.List;

public class GameData {
    private Arena arena;
    private List<Team> teams;
    private List<Round> rounds;
    private int currentRound;
    private int currentPlayer;
    private int currentTeam;
    private Config config;

    public GameData(Arena arena, List<Team> teams, List<Round> rounds, int currentRound, int currentPlayer, int currentTeam, Config config) {
        this.arena = arena;
        this.teams = teams;
        this.rounds = rounds;
        this.currentRound = currentRound;
        this.currentPlayer = currentPlayer;
        this.currentTeam = currentTeam;
        this.config = config;
    }

    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public int getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(int currentTeam) {
        this.currentTeam = currentTeam;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
