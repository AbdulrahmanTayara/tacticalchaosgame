package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveVayneAbilityMove extends Move{
    Champion attacker , target;

    public ActiveVayneAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        int maxHealth = target.getAttributes().getHealthCapacity();
        int magicResist = target.getAttributes().getMagicResist();
        Attributes attributes = new Attributes();
        int abilityDamage =(maxHealth * 15/100 + (maxHealth * 15/100) * magicResist/100);
        attributes.setHealth(abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
