package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;
import java.util.Random;

public class NobleDamageCalculator {
    public static void Calculate(List<Champion> allArenaChampions) {
        Attributes attributes = new Attributes();
        attributes.setArmor(20);
        Random random = new Random();
        if (allArenaChampions.size() >= 3 && allArenaChampions.size() < 6) {
            allArenaChampions.get(random.nextInt(allArenaChampions.size())).addToAttributes(attributes);
        } else if (allArenaChampions.size() >= 6) {
            for (Champion champion : allArenaChampions)
                champion.addToAttributes(attributes);
        }
    }
}
