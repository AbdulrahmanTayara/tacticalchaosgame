package move;

import champion.Attributes;
import champion.Champion;

public class ActiveTalonAbilityMove extends Move{
    Champion attacker;

    public ActiveTalonAbilityMove(Champion attacker) {
        this.attacker = attacker;
        numberOfRound += 4;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int criticalStrikeChance = attacker.getAttributes().getCriticalStrikeChance();
        attributes.setCriticalStrikeChance(criticalStrikeChance);
        attacker.addToAttributes(attributes);
    }
}
