package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class BrawlerDamageCalculator {
    public static void Calculate(List<Champion> brawlers) {
        Attributes attributes = new Attributes();
        if(brawlers.size()>=2 && brawlers.size() < 4 ){
            attributes.setHealth(250);
            for(Champion champion : brawlers)
                champion.addToAttributes(attributes);
        }
        else if(brawlers.size()>=4 && brawlers.size() < 6 ){
            attributes.setHealth(500);
            for(Champion champion : brawlers)
                champion.addToAttributes(attributes);
        }
        else if(brawlers.size()>=6 ){
            attributes.setHealth(1000);
            for(Champion champion : brawlers)
                champion.addToAttributes(attributes);
        }

    }
}
