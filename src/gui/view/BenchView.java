package gui.view;

import champion.Champion;
import gui.component.DetailsTooltip;
import gui.GameFrame;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.util.List;

public class BenchView {
    private List<Champion> benchChampions;
    private ScrollPane scrollPane;
    private VBox vBox;

    public BenchView(List<Champion> benchChampions) {
        this.benchChampions = benchChampions;
        this.scrollPane = new ScrollPane();
        this.scrollPane.getStyleClass().add("theme");
        this.vBox = new VBox(10);
        this.scrollPane.setContent(this.vBox);
        this.vBox.getStyleClass().add("theme");
        this.prepareBench();
    }

    private void prepareBench() {
        this.vBox.getChildren().clear();
        this.vBox.setAlignment(Pos.CENTER);
        this.vBox.setPadding(new Insets(3));
        this.scrollPane.setOnMousePressed(e -> {
            if (GameFrame.getMovedChampion() != null) {
                this.benchChampions.add(GameFrame.getMovedChampion());
                GameFrame.setMovedChampion(null);
                GameFrame.getInstance().setCursor(Cursor.DEFAULT);
                prepareBench();
            }
        });
        int i = 0;
        for (Champion champion : benchChampions) {

            Label name = new Label(champion.getName());
            Label level = new Label(Integer.toString(champion.getAttributes().getLevel()));

            Button moveToArena = new Button("Move To Arena");
            moveToArena.setOnAction(e -> {
//                FIXME: bug if the timer runs and player didn't choose place on arena
                GameFrame.getInstance().moveChampionFromBenchToStash(champion);
                prepareBench();
            });

            Button sell = new Button("Sell");
            sell.setOnAction(e -> {
                GameFrame.getInstance().pickChampionToSell(champion);
            });

            Button details = new Button("Details");
            DetailsTooltip detailsTooltip = new DetailsTooltip(champion);
            details.setOnMousePressed(actionEvent -> {
                detailsTooltip.toggleTooltip(details, actionEvent.getScreenX(), actionEvent.getScreenY());
            });

            Separator separator = new Separator();
            separator.setPadding(new Insets(4, 2, 4, 2));

            this.vBox.getChildren().addAll(name, level, moveToArena, sell, details, separator);

        }
    }

    public Node getBenchView() {
        return this.scrollPane;
    }

}
