package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;
import util.Util;

import java.util.List;
import java.util.Random;

public class ActiveBrandAbilityMove extends Move{
    Champion attacker;
    private static int championsToHit = 3;
    ActiveBrandAbilityMove(Champion attacker){
        this.attacker = attacker;
    }
    @Override
    public void performMove() {
        List <Champion> list = Util.getEnemiesInRange(attacker.getSquare() , attacker.getAttributes().getVisionRange() , attacker.getPlayerId());
        while(championsToHit > 0 && list.size() > 0){
            Attributes attributes = new Attributes();
            championsToHit--;
            Random random = new Random();
            int randomNumber = random.nextInt(list.size() - 1);
            Champion target = list.get(randomNumber);
            int currHealth = target.getAttributes().getHealth();
            int healthCapicity = target.getAttributes().getHealthCapacity();
            int currArmor = target.getAttributes().getArmor();
            int currMagicResist = target.getAttributes().getMagicResist();
            int abilityDamage = (healthCapicity * 10)/100;
            attributes.setAbilityType(AbilityType.ability);
            attributes.setHealth(- abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setArmor(-((currArmor * 10)/100));
            attributes.setMagicResist(-((currMagicResist * 10)/100));
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
            list.remove(randomNumber);
        }
    }
}
