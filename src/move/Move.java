package move;

import champion.Attributes;

public abstract class Move {
    public int numberOfRound = 0;

    public int getNumberOfRound() {
        return numberOfRound;
    }

    public void setNumberOfRound(int numberOfRound) {
        this.numberOfRound = Math.max(0 , numberOfRound);
    }

    public abstract void performMove();
}
