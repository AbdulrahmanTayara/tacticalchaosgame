package gui.page;

import filesmanager.FileManager;
import filesmanager.IFileManger;
import game.GameData;
import gui.GameFrame;
import injector.AppInjector;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class StartPage {

    private Stage stage;

    public StartPage(Stage stage) {
        this.stage = stage;
    }

    public VBox getTheView() {
        VBox vBox = new VBox();

        Button start = new Button("Start new Game");
        start.setPrefWidth(138);
        start.setPrefHeight(38);
        start.setOnMousePressed(event -> this.handleOnPressButtonStartTheGame());

        Button load = new Button("Load Game");
        load.setPrefWidth(138);
        load.setPrefHeight(38);
        load.setOnAction(e -> loadGame());

        Button reply = new Button("Replay");
        reply.setPrefWidth(138);
        reply.setPrefHeight(38);
        reply.setOnAction(e -> replayGame());

        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(8);
        vBox.getChildren().addAll(start, load, reply);


        try {
            FileInputStream input = new FileInputStream("Data/Img/poster.jpg");
            Image image = new Image(input);
            BackgroundImage backgroundImage = new BackgroundImage(image,
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundPosition.CENTER,
                    BackgroundSize.DEFAULT);
            vBox.setBackground(new Background(backgroundImage));
        } catch (FileNotFoundException e) {

        }

        return vBox;
    }

    public Scene getScene() {
        return new Scene(getTheView(), 640, 480);
    }

    private void handleOnPressButtonStartTheGame() {
        this.stage.setScene(new SettingsPage(stage).getScene());
    }

    private File chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files (*.json)", "*json"));
        fileChooser.setInitialDirectory(new File("Data\\saved_game"));
        return fileChooser.showOpenDialog(stage);
    }

    private boolean validFile(File file) {
        return file != null && file.getAbsolutePath().contains("json");
    }

    private void loadGame() {
        File file = chooseFile();
        if (!validFile(file)) return;
        IFileManger fileManger = new FileManager();
        GameData gameData = fileManger.loadGame(file.getAbsolutePath());

        runLoadedGame(gameData);
//        AppInjector.setConfig(gameData.getConfig());
//        AppInjector.setArena(gameData.getArena());
//        AppInjector.setGame(gameData);
//        GameFrame.getInstance().runTheGame();
    }


    private void replayGame() {
        List<GameData> history = new FileManager().getRecordingStates();
        if (history == null || history.isEmpty()) return;
        Timer timer = new Timer(true);

        timer.scheduleAtFixedRate(new TimerTask() {
            int i = 0;
            List<GameData> history = new FileManager().getRecordingStates();
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if (i >= history.size()) {
                        System.exit(0);
                    }
                    GameData gameData = history.get(i);
                    gameData.getConfig().setReplyMode(true);
                    refreshTheGame(gameData);
                    ++i;
                });
            }
        }, 0, 5 * 1000);
    }

    private void runLoadedGame(GameData gameData) {
        AppInjector.setConfig(gameData.getConfig());
        AppInjector.setArena(gameData.getArena());
        AppInjector.setGame(gameData);
        GameFrame.getInstance().runTheGame();
    }

    private void refreshTheGame(GameData gameData) {
        AppInjector.setConfig(gameData.getConfig());
        AppInjector.setArena(gameData.getArena());
        AppInjector.setGame(gameData);
        GameFrame.getInstance().refreshTheGame();
    }
}
