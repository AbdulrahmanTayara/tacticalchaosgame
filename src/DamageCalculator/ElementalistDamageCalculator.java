package DamageCalculator;

import champion.Attributes;
import champion.Champion;
import player.Player;

import java.util.List;

public class ElementalistDamageCalculator {
    public static void Calculate(Player player , List<Champion> elementalists) {
        Attributes attributes = new Attributes();
        if(elementalists.size()>=2 && elementalists.size()<4){
            for(Champion champion : elementalists){
                attributes.setManaIncreasing(champion.getAttributes().getManaIncreasing() * 2);
                champion.addToAttributes(attributes);
            }
        }
        else if(elementalists.size()>=4){
            for(Champion champion : elementalists){
                attributes.setManaIncreasing(champion.getAttributes().getManaIncreasing() * 2);
                champion.addToAttributes(attributes);
            }
            player.increaseNumberOfChampionInArena(1);
        }
    }
}
