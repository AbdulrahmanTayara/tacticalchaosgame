package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveKindredAbilityMove extends Move{

    Champion attacker;

    public ActiveKindredAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List <Champion> list = Util.getAllAllies(attacker.getPlayerId());
        for(Champion champion : list){
            Attributes attributes = new Attributes();
            int attackDamage = champion.getAttributes().getAttackDamage();
            attributes.setAttackDamage(attackDamage * 10/100);
            champion.addToAttributes(attributes);
        }
    }
}
