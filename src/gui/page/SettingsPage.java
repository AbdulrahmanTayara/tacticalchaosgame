package gui.page;

import game.GameMode;
import gui.GameFrame;
import injector.AppInjector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import util.Config;

import java.util.*;

public class SettingsPage {

    private Scene scene;
    private Stage stage;

    public SettingsPage(Stage stage) {
        this.scene = new Scene(getVBox(), 640, 480);
        this.stage = stage;
    }

    public VBox getVBox() {

        Config config = AppInjector.getConfig();

        Text text = new Text("Tactical Chaos");
        text.setStyle("-fx-font-size: 32;");
        HBox hBox = new HBox();
        hBox.setStyle("-fx-padding: 16px 0px;");
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(text);

        ArrayList<VBox> vBoxArrayList = new ArrayList<>();

        Label labelArenaSize = new Label("Arena Size");
        TextField textFieldArenaSize = new TextField();
        textFieldArenaSize.setText(Integer.toString(config.getArenaSize()));
        VBox vBoxArenaSize = new VBox();
        vBoxArenaSize.getChildren().addAll(labelArenaSize, textFieldArenaSize);

//        Label labelRoundsNumber                         = new Label("Rounds Number");
//        TextField textFieldRoundsNumber                 = new TextField();
//        textFieldRoundsNumber.setText(Integer.toString(config.getRoundsNumber()));
//        VBox vBoxRoundsNumber = new VBox();
//        vBoxRoundsNumber.getChildren().addAll(labelRoundsNumber, textFieldRoundsNumber);

        Label labelBuyingRoundsNumber = new Label("Buying Rounds Number");
        TextField textFieldBuyingRoundsNumber = new TextField();
        textFieldBuyingRoundsNumber.setText(Integer.toString(config.getBuyingRoundsNumber()));
        VBox vBoxBuyingRoundsNumber = new VBox();
        vBoxBuyingRoundsNumber.getChildren().addAll(labelBuyingRoundsNumber, textFieldBuyingRoundsNumber);

        HBox hBox1 = new HBox();
        hBox1.getChildren().addAll(vBoxArenaSize, vBoxBuyingRoundsNumber);
        vBoxArrayList.add(vBoxArenaSize);
        vBoxArrayList.add(vBoxBuyingRoundsNumber);
//        vBoxArrayList.add(vBoxRoundsNumber);

        HBox.setHgrow(vBoxArenaSize, Priority.ALWAYS);
        vBoxArenaSize.setMaxWidth(Double.MAX_VALUE);
//        HBox.setHgrow(vBoxRoundsNumber, Priority.ALWAYS);
//        vBoxRoundsNumber.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxBuyingRoundsNumber, Priority.ALWAYS);
        vBoxBuyingRoundsNumber.setMaxWidth(Double.MAX_VALUE);


        Label labelPlayerChampionsInArena = new Label("Player Champions InArena");
        TextField textFieldPlayerChampionsInArena = new TextField();
        textFieldPlayerChampionsInArena.setText(Integer.toString(config.getPlayerChampionsInArena()));
        VBox vBoxPlayerChampionsInArena = new VBox();
        vBoxPlayerChampionsInArena.getChildren().addAll(labelPlayerChampionsInArena, textFieldPlayerChampionsInArena);

        Label labelPlayerChampionsInBench = new Label("Player Champions InBench");
        TextField textFieldPlayerChampionsInBench = new TextField();
        textFieldPlayerChampionsInBench.setText(Integer.toString(config.getPlayerChampionsInBench()));
        VBox vBoxPlayerChampionsInBench = new VBox();
        vBoxPlayerChampionsInBench.getChildren().addAll(labelPlayerChampionsInBench, textFieldPlayerChampionsInBench);

        Label labelRandomStoreListSize = new Label("Random Store List Size");
        TextField textFieldRandomStoreListSize = new TextField();
        textFieldRandomStoreListSize.setText(Integer.toString(config.getRandomStoreListSize()));
        VBox vBoxRandomStoreListSize = new VBox();
        vBoxRandomStoreListSize.getChildren().addAll(labelRandomStoreListSize, textFieldRandomStoreListSize);

        HBox hBox2 = new HBox();
        hBox2.getChildren().addAll(vBoxPlayerChampionsInArena, vBoxPlayerChampionsInBench, vBoxRandomStoreListSize);
        vBoxArrayList.add(vBoxPlayerChampionsInArena);
        vBoxArrayList.add(vBoxPlayerChampionsInBench);
        vBoxArrayList.add(vBoxRandomStoreListSize);

        HBox.setHgrow(vBoxPlayerChampionsInArena, Priority.ALWAYS);
        vBoxPlayerChampionsInArena.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxPlayerChampionsInBench, Priority.ALWAYS);
        vBoxPlayerChampionsInBench.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxRandomStoreListSize, Priority.ALWAYS);
        vBoxRandomStoreListSize.setMaxWidth(Double.MAX_VALUE);


        Label labelMaxChampionInStore = new Label("Max Champion In Store");
        TextField textFieldMaxChampionInStore = new TextField();
        textFieldMaxChampionInStore.setText(Integer.toString(config.getMaxChampionInStore()));
        VBox vBoxMaxChampionInStore = new VBox();
        vBoxMaxChampionInStore.getChildren().addAll(labelMaxChampionInStore, textFieldMaxChampionInStore);

        Label labelNumberOfCoinsEachRound = new Label("Number Of Coins Each Round");
        TextField textFieldNumberOfCoinsEachRound = new TextField();
        textFieldNumberOfCoinsEachRound.setText(Integer.toString(config.getNumberOfCoinsEachRound()));
        VBox vBoxNumberOfCoinsEachRound = new VBox();
        vBoxNumberOfCoinsEachRound.getChildren().addAll(labelNumberOfCoinsEachRound, textFieldNumberOfCoinsEachRound);

        Label labelChampionMaxLevel = new Label("Champion Max Level");
        TextField textFieldChampionMaxLevel = new TextField();
        textFieldChampionMaxLevel.setText(Integer.toString(config.getChampionMaxLevel()));
        VBox vBoxChampionMaxLevel = new VBox();
        vBoxChampionMaxLevel.getChildren().addAll(labelChampionMaxLevel, textFieldChampionMaxLevel);

        HBox hBox3 = new HBox();
        hBox3.getChildren().addAll(vBoxMaxChampionInStore, vBoxNumberOfCoinsEachRound, vBoxChampionMaxLevel);
        vBoxArrayList.add(vBoxMaxChampionInStore);
        vBoxArrayList.add(vBoxNumberOfCoinsEachRound);
        vBoxArrayList.add(vBoxChampionMaxLevel);

        HBox.setHgrow(vBoxMaxChampionInStore, Priority.ALWAYS);
        vBoxMaxChampionInStore.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxNumberOfCoinsEachRound, Priority.ALWAYS);
        vBoxNumberOfCoinsEachRound.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxChampionMaxLevel, Priority.ALWAYS);
        vBoxChampionMaxLevel.setMaxWidth(Double.MAX_VALUE);


        Label labelMaxSwaps = new Label("Max Swaps");
        TextField textFieldMaxSwaps = new TextField();
        textFieldMaxSwaps.setText(Integer.toString(config.getMaxSwaps()));
        VBox vBoxMaxSwaps = new VBox();
        vBoxMaxSwaps.getChildren().addAll(labelMaxSwaps, textFieldMaxSwaps);


        Label labelAcceptedClasses = new Label("Accepted Classes");
        List<String> acceptedClasses = new ArrayList<>();
        for (int i = 0; i < config.getAcceptedClasses().size(); i++) {
            acceptedClasses.add(config.getAcceptedClasses().get(i).getValue());
        }

        ObservableList<String> acceptedClassesList = FXCollections.observableList(acceptedClasses);
        ListView<String> listView = new ListView<>(acceptedClassesList);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setMaxHeight(50);

        VBox vBoxAcceptedClasses = new VBox();
        vBoxAcceptedClasses.getChildren().addAll(labelAcceptedClasses, listView);

        Label labelMaxRounds = new Label("Max Rounds");
        TextField textFieldMaxRounds = new TextField();
        textFieldMaxRounds.setText(Integer.toString(config.getMaxRounds()));
        VBox vBoxMaxRounds = new VBox();
        vBoxMaxRounds.getChildren().addAll(labelMaxRounds, textFieldMaxRounds);

        HBox hBox4 = new HBox();
        hBox4.getChildren().addAll(vBoxMaxSwaps, vBoxAcceptedClasses, vBoxMaxRounds);
        vBoxArrayList.add(vBoxMaxSwaps);
        vBoxArrayList.add(vBoxAcceptedClasses);
        vBoxArrayList.add(vBoxMaxRounds);

        HBox.setHgrow(vBoxMaxSwaps, Priority.ALWAYS);
        vBoxMaxSwaps.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxAcceptedClasses, Priority.ALWAYS);
        vBoxAcceptedClasses.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxMaxRounds, Priority.ALWAYS);
        vBoxMaxRounds.setMaxWidth(Double.MAX_VALUE);


        Label labelMaxOrdersEachRound = new Label("Max Orders Each Round");
        TextField textFieldMaxOrdersEachRound = new TextField();
        textFieldMaxOrdersEachRound.setText(Integer.toString(config.getMaxOrdersEachRound()));
        VBox vBoxMaxOrdersEachRound = new VBox();
        vBoxMaxOrdersEachRound.getChildren().addAll(labelMaxOrdersEachRound, textFieldMaxOrdersEachRound);

        Label labelNumberOfPlayers = new Label("Number Of Players");
        TextField textFieldNumberOfPlayers = new TextField();
        textFieldNumberOfPlayers.setText(Integer.toString(config.getNumberOfPlayers()));
        VBox vBoxNumberOfPlayers = new VBox();
        vBoxNumberOfPlayers.getChildren().addAll(labelNumberOfPlayers, textFieldNumberOfPlayers);

        Label labelNumberOfBots = new Label("Number Of Bots");
        TextField textFieldNumberOfBots = new TextField();
        textFieldNumberOfBots.setText(Integer.toString(config.getNumberOfBots()));
        VBox vBoxNumberOfBots = new VBox();
        vBoxNumberOfBots.getChildren().addAll(labelNumberOfBots, textFieldNumberOfBots);

        HBox hBox5 = new HBox();
        hBox5.getChildren().addAll(vBoxMaxOrdersEachRound, vBoxNumberOfPlayers, vBoxNumberOfBots);
        vBoxArrayList.add(vBoxMaxOrdersEachRound);
        vBoxArrayList.add(vBoxNumberOfPlayers);
        vBoxArrayList.add(vBoxNumberOfBots);

        HBox.setHgrow(vBoxMaxOrdersEachRound, Priority.ALWAYS);
        vBoxMaxOrdersEachRound.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxNumberOfPlayers, Priority.ALWAYS);
        vBoxNumberOfPlayers.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(vBoxNumberOfBots, Priority.ALWAYS);
        vBoxNumberOfBots.setMaxWidth(Double.MAX_VALUE);

        for (VBox vBox1 : vBoxArrayList) {
            vBox1.setStyle("-fx-padding: 8px;");
        }

        Button button = new Button("Continue");
        button.setPrefWidth(138);
        button.setPrefHeight(38);

        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().addAll("Default", "Light", "Dark");
        choiceBox.setValue("Default");

        HBox hBox6 = new HBox();
        hBox6.getChildren().addAll(button, choiceBox);
        hBox6.setStyle("-fx-padding: 16px 0px");
        hBox6.setAlignment(Pos.CENTER);

        button.setOnAction(event -> {
            Config config1 = new Config(
                    Integer.parseInt(textFieldArenaSize.getText()),
                    Integer.parseInt(textFieldBuyingRoundsNumber.getText()),
                    Integer.parseInt(textFieldPlayerChampionsInArena.getText()),
                    Integer.parseInt(textFieldPlayerChampionsInBench.getText()),
                    Integer.parseInt(textFieldRandomStoreListSize.getText()),
                    Integer.parseInt(textFieldMaxChampionInStore.getText()),
                    Integer.parseInt(textFieldNumberOfCoinsEachRound.getText()),
                    Integer.parseInt(textFieldChampionMaxLevel.getText()),
                    Integer.parseInt(textFieldMaxSwaps.getText()),
                    new ArrayList<>(listView.getSelectionModel().getSelectedItems()),
                    Integer.parseInt(textFieldMaxRounds.getText()),
                    Integer.parseInt(textFieldMaxOrdersEachRound.getText()),
                    Integer.parseInt(textFieldNumberOfPlayers.getText()),
                    Integer.parseInt(textFieldNumberOfBots.getText()),
                    choiceBox.getValue()
            );
            config1.setGameMode(GameMode.GUI.toString());
            AppInjector.setConfig(config1);
//            GameFrame.getInstance().runTheGame();
            this.stage.setScene(new TeamPickerPage(AppInjector.getConfig().getNumberOfPlayers(), AppInjector.getConfig().getNumberOfBots()).getScene());
        });


        VBox vBox = new VBox();
        vBox.getChildren().addAll(hBox, hBox1, hBox2, hBox3, hBox4, hBox5, hBox6);

//        try {
//            FileInputStream input = new FileInputStream("C:\\\\Users\\\\hp\\\\Desktop\\\\Java tests\\\\tacticalchaosgame\\\\poster.jpg");
//            Image image = new Image(input);
//            BackgroundImage backgroundImage = new BackgroundImage(image,
//                    BackgroundRepeat.NO_REPEAT,
//                    BackgroundRepeat.NO_REPEAT,
//                    BackgroundPosition.DEFAULT,
//                    BackgroundSize.DEFAULT);
//            vBox.setBackground(new Background(backgroundImage));
//        } catch (FileNotFoundException e) {
//
//        }


        return vBox;
    }

    public Scene getScene() {
        return scene;
    }
}
