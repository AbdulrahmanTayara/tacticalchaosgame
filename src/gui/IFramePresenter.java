package gui;

import game.IGamePresenter;
import player.Player;

public interface IFramePresenter extends IGamePresenter {
    Player getPlayer();
}
