package champion;

public class Attributes {
    // Champion Attributes

    //// basic

    private int health;
    private int healthCapacity;
    private int armor;
    private int magicResist;
    private int visionRange;
    private int attackRange;
    private int attackDamage;
    private int movementSpeed;
    private int criticalStrikeChance;
    private int criticalStrikeDamage;
    private int manaStart;
    private double manaCost;

    private int mana = 0;
    private int stunDuration = 0;
    private int immunityDuration = 0;
    private int immunityBasicAttackDuration = 0;
    private int level = 1;

    //additional
    private boolean isDead = false;
    private boolean trueDamage = false;
    private boolean isVisible = true;
    private double missTheAttack = 0;
    private int hitExtraTimechance = 0;
    private int numberOfExtraAttacks = 0;
    private int manaIncreasing = 1;
    private int hitExtraTergetschance = 0;
    private int numberOfExtraTergets = 0;
    private int lessDamageChance = 0;
    private int extraHealthOnTransformation = 0;
    private int abilityPower = 0;

    //abilityType of attribute
    AbilityType abilityType;

    public Attributes() {
    }

    // basic attributes

    public Attributes(int health, int armor, int magicResist, int visionRange, int attackRange, int attackDamage, int movementSpeed, int criticalStrikeChance, int criticalStrikeDamage, int manaStart, double manaCost) {
        this.health = health;
        this.healthCapacity = health;
        this.armor = armor;
        this.magicResist = magicResist;
        this.visionRange = visionRange;
        this.attackRange = attackRange;
        this.attackDamage = attackDamage;
        this.movementSpeed = movementSpeed;
        this.criticalStrikeChance = criticalStrikeChance;
        this.criticalStrikeDamage = criticalStrikeDamage;
        this.manaStart = manaStart;
        this.manaCost = manaCost;
    }


    // all attributes

    public Attributes(int health, int healthCapacity, int armor, int magicResist, int visionRange, int attackRange, int attackDamage, int movementSpeed, int criticalStrikeChance, int criticalStrikeDamage, int manaStart, double manaCost, int mana, int stunDuration, int immunityDuration, int immunityBasicAttackDuration , AbilityType abilityType) {
        this.health = health;
        this.healthCapacity = healthCapacity;
        this.armor = armor;
        this.magicResist = magicResist;
        this.visionRange = visionRange;
        this.attackRange = attackRange;
        this.attackDamage = attackDamage;
        this.movementSpeed = movementSpeed;
        this.criticalStrikeChance = criticalStrikeChance;
        this.criticalStrikeDamage = criticalStrikeDamage;
        this.manaStart = manaStart;
        this.manaCost = manaCost;
        this.mana = mana;
        this.stunDuration = stunDuration;
        this.immunityDuration = immunityDuration;
        this.immunityBasicAttackDuration = immunityBasicAttackDuration;
        this.abilityType = abilityType;
    }

    public void merge(Attributes attributes) {
        this.health += attributes.getHealth();
        this.healthCapacity += attributes.getHealthCapacity();
        this.armor += attributes.getArmor();
        this.magicResist += attributes.getMagicResist();
        this.visionRange += attributes.getVisionRange();
        this.attackRange += attributes.getAttackRange();
        this.attackDamage += attributes.getAttackDamage();
        this.movementSpeed += attributes.getMovementSpeed();
        this.criticalStrikeChance += attributes.getCriticalStrikeChance();
        this.criticalStrikeDamage += attributes.getCriticalStrikeDamage();
        this.manaStart += attributes.getManaStart();
        this.manaCost += attributes.getManaCost();
        this.mana += attributes.getMana();
        this.stunDuration += attributes.getStunDuration();
        this.immunityDuration += attributes.getImmunityDuration();
        this.immunityBasicAttackDuration += attributes.getImmunityBasicAttackDuration();
        trueDamage = attributes.isTrueDamage();
        missTheAttack += attributes.getMissTheAttack();
        hitExtraTimechance += attributes.getHitExtraTimechance();
        numberOfExtraAttacks += attributes.getNumberOfExtraAttacks();
        manaIncreasing = attributes.getManaIncreasing();
        hitExtraTergetschance += attributes.getHitExtraTergetschance();
        numberOfExtraTergets += attributes.getNumberOfExtraTergets();
        lessDamageChance += attributes.getLessDamageChance();
        extraHealthOnTransformation += attributes.getExtraHealthOnTransformation();
    }

    public int getMana() {
        return mana;
    }

    public int getStunDuration() {
        return stunDuration;
    }

    public void setStunDuration(int stunDuration) {
        this.stunDuration = Math.max(0, stunDuration);
    }

    public int getImmunityDuration() {
        return immunityDuration;
    }



    public void setImmunityDuration(int immunityDuration) {
        this.immunityDuration = Math.max(0, immunityDuration);
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = Math.max(0, armor);
    }

    public int getMagicResist() {
        return magicResist;
    }

    public void setMagicResist(int magicResist) {
        this.magicResist = Math.max(0, magicResist);
    }

    public int getVisionRange() {
        return visionRange;
    }

    public void setVisionRange(int visionRange) {
        this.visionRange = visionRange;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(int attackRange) {
        this.attackRange = attackRange;
    }

    public int getAttackDamage() {
        return attackDamage;
    }

    public void setAttackDamage(int attackDamage) {
        this.attackDamage = attackDamage;
    }

    public int getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public int getCriticalStrikeChance() {
        return criticalStrikeChance;
    }

    public void setCriticalStrikeChance(int criticalStrikeChance) {
        this.criticalStrikeChance = criticalStrikeChance;
    }

    public int getCriticalStrikeDamage() {
        return criticalStrikeDamage;
    }

    public void setCriticalStrikeDamage(int criticalStrikeDamage) {
        this.criticalStrikeDamage = criticalStrikeDamage;
    }

    public int getManaStart() {
        return manaStart;
    }

    public void setManaStart(int manaStart) {
        this.manaStart = manaStart;
    }

    public double getManaCost() {
        return manaCost;
    }

    public void setManaCost(double manaCost) {
        this.manaCost = manaCost;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealthCapacity() {
        return healthCapacity;
    }

    public void setHealthCapacity(int healthCapacity) {
        this.healthCapacity = healthCapacity;
    }

    public int getImmunityBasicAttackDuration() {
        return immunityBasicAttackDuration;
    }

    public void setImmunityBasicAttackDuration(int immunityBasicAttackDuration) {
        this.immunityBasicAttackDuration = immunityBasicAttackDuration;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public boolean isTrueDamage() {
        return trueDamage;
    }

    public void setTrueDamage(boolean trueDamage) {
        this.trueDamage = trueDamage;
    }

    public double getMissTheAttack() {
        return missTheAttack;
    }

    public int getAbilityPower() {
        return abilityPower;
    }

    public void setAbilityPower(int abilityPower) {
        this.abilityPower = abilityPower;
    }

    public AbilityType getAbilityType() {
        return abilityType;
    }

    public void setAbilityType(AbilityType abilityType) {
        this.abilityType = abilityType;
    }

    public void setMissTheAttack(double missTheAttack) {
        this.missTheAttack = missTheAttack;
    }

    public int getHitExtraTimechance() {
        return hitExtraTimechance;
    }

    public void setHitExtraTimechance(int hitExtraTimechance) {
        this.hitExtraTimechance = hitExtraTimechance;
    }

    public int getNumberOfExtraAttacks() {
        return numberOfExtraAttacks;
    }

    public void setNumberOfExtraAttacks(int numberOfExtraAttacks) {
        this.numberOfExtraAttacks = numberOfExtraAttacks;
    }

    public int getManaIncreasing() {
        return manaIncreasing;
    }

    public void setManaIncreasing(int manaIncreasing) {
        this.manaIncreasing = manaIncreasing;
    }

    public int getHitExtraTergetschance() {
        return hitExtraTergetschance;
    }

    public void setHitExtraTergetschance(int hitExtraTergetschance) {
        this.hitExtraTergetschance = hitExtraTergetschance;
    }

    public int getNumberOfExtraTergets() {
        return numberOfExtraTergets;
    }

    public void setNumberOfExtraTergets(int numberOfExtraTergets) {
        this.numberOfExtraTergets = numberOfExtraTergets;
    }

    public int getLessDamageChance() {
        return lessDamageChance;
    }

    public void setLessDamageChance(int lessDamageChance) {
        this.lessDamageChance = lessDamageChance;
    }

    public int getExtraHealthOnTransformation() {
        return extraHealthOnTransformation;
    }

    public void setExtraHealthOnTransformation(int extraHealthOnTransformation) {
        this.extraHealthOnTransformation = extraHealthOnTransformation;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
