package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveDariusAbilityMove extends Move{

    Champion attacker;
    private static double radius = 20.0;
    private static int abilityDamage = 75;

    ActiveDariusAbilityMove(Champion attacker){
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List <Champion> list = Util.getEnemiesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setAbilityType(AbilityType.ability);
            attributes.setTrueDamage(true);
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.merge(attacker.getAttackAttributes());
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
        Attributes attributes = new Attributes();
        attributes.setHealth(abilityDamage * list.size());
        attacker.addToAttributes(attributes);
    }
}
