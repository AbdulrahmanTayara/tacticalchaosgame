package move;

import arena.Square;
import champion.AbilityType;
import champion.Attributes;
import champion.Champion;

public class ActiveEvelynnAbilityMove extends Move {

    Champion attacker , target;
    private static int inf = Integer.MAX_VALUE;

    public ActiveEvelynnAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int maxTaregetHealth = target.getAttributes().getHealthCapacity();
        int currHealth = target.getAttributes().getHealth();
        if(currHealth <= (maxTaregetHealth * 40)/100)  attributes.setHealth(-inf);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        attributes = new Attributes();
        Square square = attacker.getSquare();
        attacker.setSquare(new Square(square.getX() - 20 , square.getY()));
        attacker.addToAttributes(attributes);
    }
}
