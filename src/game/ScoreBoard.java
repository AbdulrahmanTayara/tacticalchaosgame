package game;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ScoreBoard {

    private final String FILE_NAME = "Data\\scoreboard.txt";

    private BufferedWriter writer;
    private BufferedReader reader;

    public ScoreBoard() {
        try {
            writer = new BufferedWriter(new FileWriter(FILE_NAME));
            reader = new BufferedReader(new FileReader(FILE_NAME));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addEvent(String event) {
        try {
            writer.write(event);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getEvents() {
        List<String> events = new ArrayList<>();
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                events.add(line);
            }
        } catch (IOException ignored) {
            return events;
        }
        return events;
    }
}
