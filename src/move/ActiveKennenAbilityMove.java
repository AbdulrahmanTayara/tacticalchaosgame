package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveKennenAbilityMove extends Move{
    Champion attacker , target;

    public ActiveKennenAbilityMove(Champion attacker, Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        int attackDamage = attacker.getAttributes().getAttackDamage();
        int abilityDamage = attackDamage * 350 / 100;
        Attributes attributes = new Attributes();
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
