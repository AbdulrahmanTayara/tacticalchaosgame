package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class VoidHit extends Weapon{

    public VoidHit() {
        super(WeaponType.VOID_HIT.name(),"Void Hit");
        super.setChampionClass(ChampionClass.Void);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setHealthCapacity(5);
        return attributes;
    }
}
