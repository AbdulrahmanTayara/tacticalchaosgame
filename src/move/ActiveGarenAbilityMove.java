package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;

public class ActiveGarenAbilityMove extends Move{
    Champion attacker , target;

    public ActiveGarenAbilityMove(Champion attacker, Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        int currHealth = attacker.getAttributes().getHealth();
        int healthCapacity = attacker.getAttributes().getHealthCapacity();
        int missingHealth = healthCapacity - currHealth;
        Attributes attributes = new Attributes();
        int abilityDamage = missingHealth;
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
