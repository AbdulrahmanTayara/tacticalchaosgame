package round;

import DamageCalculator.DamageCalculatorFactory;
import injector.AppInjector;
import player.Player;

public class Planning extends Round {

    public Planning() {
        super(RoundType.PLANNING.name());
    }

    @Override
    public void start(Player player) {
        player.setNumberOfPermissibleSwaps(AppInjector.getConfig().getMaxSwaps());
        player.startPlanningRound();
        //DamageCalculatorFactory.CalculateClasses(player);
    }

}
