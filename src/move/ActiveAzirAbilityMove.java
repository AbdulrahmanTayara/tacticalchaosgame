package move;

import arena.Square;
import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import injector.AppInjector;
import util.Util;

public class ActiveAzirAbilityMove extends Move{

    Champion attacker;

    public ActiveAzirAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        // in GUI we need to put 2 champs in this squares
        Square oppositeSquare = Util.getOppositeSquare(attacker.getSquare());
        Champion target = Util.getNearestEnemy(oppositeSquare , attacker.getAttributes().getAttackRange(), attacker.getPlayerId());
        int attackDamage = attacker.getAttributes().getAttackDamage();
        Attributes attributes = new Attributes();
        attributes.setHealth(-attackDamage + (attacker.getAttributes().getAbilityPower() * attackDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        Square anotherOppositeSquare = new Square(oppositeSquare.getX() , AppInjector.getConfig().getArenaSize() - oppositeSquare.getY());
        target = Util.getNearestEnemy(anotherOppositeSquare , attacker.getAttributes().getAttackRange() , attacker.getPlayerId());
        attributes = new Attributes();
        attributes.setHealth(-attackDamage - (attacker.getAttributes().getAbilityPower() * attackDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
