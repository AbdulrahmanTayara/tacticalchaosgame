package gui.component;

import arena.ArenaSquare;
import arena.Square;
import champion.Champion;
import gui.GameFrame;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.util.Duration;
import util.Util;

import java.util.ArrayList;
import java.util.List;

public class GuiSquare extends Button {

    private ArenaSquare arenaSquare;
    private Square square;
    private String lastStyle = "";

    public GuiSquare() {
        super();
        this.getStyleClass().add("gui-square");
    }

    public GuiSquare(int x, int y, ArenaSquare arenaSquare) {
        this();
        this.square = new Square(x, y);
        this.arenaSquare = arenaSquare;
        this.setMaxWidth(Double.MAX_VALUE);
        this.setMaxHeight(Double.MAX_VALUE);

        GridPane.setFillWidth(this, true);
        GridPane.setFillHeight(this, true);
        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);

        this.initColor();
    }

    public List<Champion> getChampions() {
        return filterByPlayer();
    }

    public void setChampions(List<Champion> champions) {
        this.arenaSquare.setChampions(champions);
    }

    public ArenaSquare getArenaSquare() {
        return arenaSquare;
    }

    public void setArenaSquare(ArenaSquare arenaSquare) {
        this.arenaSquare = arenaSquare;
    }

    public Square getSquare() {
        return square;
    }

    public void setSquare(Square square) {
        this.square = square;
    }

    public void refreshSquare () {
        boolean isPlanning = GameFrame.getInstance().isPlanning();
        this.setStyle("-fx-font-size: 18;");
        String weapon = this.arenaSquare.getWeapons().isEmpty() ? "" : "🏹";
        weaponTooltip();
        List <Champion> championList = isPlanning ? this.arenaSquare.getChampions() : this.filterByPlayer();
        if(championList.size() > 1) {
            this.setText("\uD83D\uDC65 " + weapon);
        } else if (championList.size() == 1) {
            String playerColor = Util.getPlayerById(this.arenaSquare.getChampions().get(0).getPlayerId()).getColor();
            this.appendStyle("-fx-border-width: 5; -fx-border-color: #" + playerColor);
            this.setText(this.getChampions().get(0).getName() + " " + weapon);
        } else {
            this.setText(weapon);
        }
        initColor();
    }

    private void weaponTooltip () {
        if (!this.arenaSquare.getWeapons().isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder();
            this.arenaSquare.getWeapons().forEach(weapon -> {
                stringBuilder.append(weapon.getName()).append('\n');
            });
            Tooltip tooltip = new Tooltip(stringBuilder.toString());
            tooltip.setShowDelay(Duration.ZERO);
            tooltip.setHideDelay(Duration.ZERO);
            this.setTooltip(tooltip);
        }
    }

    private List<Champion> filterByPlayer () {
        if (GameFrame.getInstance().isPlanning()) {
            return this.arenaSquare.getChampions();
        }
        List<Champion> championList = new ArrayList<>(this.arenaSquare.getChampions());
        int currentPlayer = GameFrame.getInstance().getPresenter().getPlayer().getId();
        championList.removeIf(champion -> champion.getPlayerId() != currentPlayer);
        return championList;
    }

    private void appendStyle (String style) {
        this.setStyle(this.getStyle() + style);
    }

    private void initColor () {
        try {
            getStyleClass().remove(lastStyle);
        } catch (Exception ignored) {}
        switch (this.arenaSquare.getSquareType()) {
            case Water:
                lastStyle = "water-square";
                break;
            case Terrain:
                lastStyle = "terrain-square";
                break;
            case Grass:
                lastStyle = "grass-square";
                break;
            case Standard:
                lastStyle = "standard-square";
                break;
        }
        getStyleClass().add(lastStyle);
    }

}












//        this.setStyle("-fx-background-image: url('');");
//        this.setStyle("-fx-background-image: url('/Data/Img/Squares/grass.jpg')");

//        TODO: uncomment for images xD
//        FileInputStream fileInputStream = null;
//        try {
//            fileInputStream = new FileInputStream("Data/Img/Squares/" + this.arenaSquare.getSquareType().toString().toLowerCase() + ".jpg");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        BackgroundImage backgroundImage = new BackgroundImage(
//                new Image(fileInputStream),
//                BackgroundRepeat.NO_REPEAT,
//                BackgroundRepeat.NO_REPEAT,
//                BackgroundPosition.DEFAULT,
//                BackgroundSize.DEFAULT
//        );
//        Background background = new Background(backgroundImage);
//        this.setBackground(background);