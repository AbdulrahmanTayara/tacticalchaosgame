package arenaobservable;

import arena.Square;
import champion.Champion;

public interface ArenaObserver {
    void notify(Champion champion, Square newSquare);
    void notifyDead(Champion champion);
}
