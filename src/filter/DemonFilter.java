package filter;

import champion.Champion;
import champion.ChampionClass;

public class DemonFilter extends FilterDecorator {

    public DemonFilter () {

    }

    public DemonFilter(Filter filter) {
        this.filter = filter;
    }

    @Override
    public boolean isAccepted(Champion champion) {
        for (ChampionClass championClass: champion.getChampionClasses()) {
            if (championClass.toString().equals("Demon")) {
                return true;
            }
        }
        return false;
    }
}
