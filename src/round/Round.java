package round;

import com.google.gson.annotations.SerializedName;
import player.Player;

public abstract class Round {
    @SerializedName("roundType")
    private String roundType;

    public Round(String roundType) {
        this.roundType = roundType;
    }

    public abstract void start(Player player);
}
