package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveShenAbilityMove extends Move{
    private static double radius = 20;
    Champion attacker;

    public ActiveShenAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getAlliesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion champion : list){
            Attributes attributes = new Attributes();
            attributes.setImmunityBasicAttackDuration(1);
            champion.addToAttributes(attributes);
        }
    }


}
