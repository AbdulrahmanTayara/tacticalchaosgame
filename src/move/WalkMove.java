package move;

import arena.Square;
import champion.Attributes;
import champion.Champion;

public class WalkMove extends Move{

    Champion champion;
    Direction direction;
    public WalkMove(Champion champion , Direction direction){
        this.champion = champion;
        this.direction = direction;
    }
    @Override
    public void performMove() {
        int movementSpeed = champion.getAttributes().getMovementSpeed();
        Attributes attributes = new Attributes();
        Square square = champion.getSquare();
        champion.setSquare(new Square((square.getX() + movementSpeed * direction.getX()) , (square.getY() + movementSpeed * direction.getY())));
        champion.addToAttributes(attributes);
        attributes = new Attributes();
        attributes.setHealth(champion.getAttributes().getExtraHealthOnTransformation());
        champion.addToAttributes(attributes);
    }
}
