package move;

import arena.Square;
import champion.Attributes;
import champion.Champion;
import util.Util;

public class ActiveZedAbilityMove extends Move{

    Champion attacker;
    // here in GUI we need to put the same champion on the opposite square
    public ActiveZedAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Square oppositeSquare = Util.getOppositeSquare(attacker.getSquare());
        Champion target = Util.getNearestEnemy(oppositeSquare , attacker.getAttributes().getAttackRange() , attacker.getPlayerId());
        Attributes attributes = new Attributes();
        int abilityDamage = attacker.getAttributes().getAttackDamage();
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
