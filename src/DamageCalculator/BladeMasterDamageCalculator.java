package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class BladeMasterDamageCalculator {
    public static void Calculate(List<Champion> bladeMasters) {
        Attributes attributes = new Attributes();
        attributes.setHitExtraTimechance(40);
        if (bladeMasters.size() >= 3 && bladeMasters.size() < 6) {
            attributes.setNumberOfExtraAttacks(1);
            for (Champion champion : bladeMasters) {
                champion.addToAttributes(attributes);
            }
        } else if (bladeMasters.size() >= 6 && bladeMasters.size() < 9) {
            attributes.setNumberOfExtraAttacks(2);
            for (Champion champion : bladeMasters) {
                champion.addToAttributes(attributes);
            }
        } else if (bladeMasters.size() >= 9) {
            attributes.setNumberOfExtraAttacks(4);
            for (Champion champion : bladeMasters) {
                champion.addToAttributes(attributes);
            }
        }
    }
}
