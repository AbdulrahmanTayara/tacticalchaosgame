package gui.view;

import champion.Champion;
import gui.GameFrame;
import gui.component.GuiSquare;
import injector.AppInjector;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import player.Player;

import java.util.ArrayList;
import java.util.List;

public class ArenaView {

//    todo: buying round player can see only his champions .. planning round player can see only champions in his vision range
    private GridPane gridPane;
    private ScrollPane scrollPane;
    private List<GuiSquare> listOfGuiSquares;
    public ArenaView() {
        this.listOfGuiSquares = new ArrayList<>();
        this.scrollPane = new ScrollPane();
        this.gridPane = this.makeGridPane();
        scrollPane.setContent(gridPane);
        this.scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
    }

    private GridPane makeGridPane() {
        int gridDim = AppInjector.getConfig().getArenaSize();
        GridPane gridPane = new GridPane();
        gridPane.setVgap(3);
        gridPane.setHgap(3);
        for (int i = 0; i < gridDim; i++) {
            for (int j = 0; j < gridDim; j++) {
                GuiSquare guiSquare = new GuiSquare(i, j, AppInjector.getArena().getArenaSquare(i, j));

                guiSquare.setOnAction(e -> {
                    GameFrame.setClickedButton(guiSquare);
                    if (GameFrame.getMovedChampion() != null) {
//                        guiSquare.addChampion(GameFrame.getMovedChampion());
                        guiSquare.refreshSquare();
                        GameFrame.getInstance().moveChampionFromStashToArena();
                    } else {
                        if (guiSquare.getChampions().size() == 1) {
                            GameFrame.getInstance().getRunningGamePage().openChampionView(
                                    guiSquare.getChampions().get(0)
                            );
                        } else if (guiSquare.getChampions().size() > 1) {
                            Stage myStage = new Stage();
                            VBox myVBox = new VBox(10);
                            myVBox.setPadding(new Insets(25));
                            myVBox.setAlignment(Pos.CENTER);
                            for (Champion myChampion:guiSquare.getChampions()) {
                                Button myButton = new Button(myChampion.getName());
                                myButton.setOnAction(t -> {
                                    GameFrame.getInstance().getRunningGamePage().openChampionView(myChampion);
                                    myStage.close();
                                });
                                myVBox.getChildren().add(myButton);
                            }
                            Scene myScene = new Scene(myVBox);
                            myStage.setScene(myScene);
                            myStage.show();
                        }
                    }
                });

                listOfGuiSquares.add(guiSquare);
                gridPane.add(guiSquare, j, i);
            }
        }
        return gridPane;
    }

    public void refreshArenaView () {
        listOfGuiSquares.forEach(GuiSquare::refreshSquare);
    }

    public Node getArenaView() {
        return this.scrollPane;
    }
}
