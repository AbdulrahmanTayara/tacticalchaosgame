package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class DragonDamageCalculator {
    public static void Calculate(List<Champion> dragons) {
        if(dragons.size()>=2){
            Attributes attributes = new Attributes();
            attributes.setImmunityDuration(1);
            for(Champion champion : dragons)
                champion.addToAttributes(attributes);
        }

    }
}
