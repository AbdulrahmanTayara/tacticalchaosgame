package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class SorcererDamageCalculator {
    // nam al nana la ank kteeeeer ghaleeeth
    public static void Calculate(List<Champion> sorcerers) {
        Attributes attributes = new Attributes();
        if(sorcerers.size()>=3 && sorcerers.size() <6 ){
            attributes.setAbilityPower(40);
            for(Champion champion : sorcerers){
                champion.addToAttributes(attributes);
            }
        }
        else if(sorcerers.size()>=6 && sorcerers.size() <9 ){
            attributes.setAbilityPower(70);
            for(Champion champion : sorcerers){
                champion.addToAttributes(attributes);
            }
        }
        else if(sorcerers.size()>=9 ){
            attributes.setAbilityPower(100);
            for(Champion champion : sorcerers){
                champion.addToAttributes(attributes);
            }
        }
    }

}