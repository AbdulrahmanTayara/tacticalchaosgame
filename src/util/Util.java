package util;

import arena.Square;
import champion.Champion;
import champion.ChampionClass;
import injector.AppInjector;
import move.Direction;
import player.Player;
import player.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Util {

    //private static List<Player> getPlayers();
    private static List<Team> teams;

//    public static void setgetPlayers()(List<Player> getPlayers()) {
//        Util.getPlayers() = getPlayers();
//    }

    public static void setTeams(List<Team> teams) {
        Util.teams = teams;
    }

    private static List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        teams.forEach(team -> players.addAll(team.getPlayersInTeam()));
        return players;
    }

    private static boolean inRange(Square source, Square target, double radius) {
        return (Math.pow((source.getX() - target.getX()), 2) +
                Math.pow((source.getY() - target.getY()), 2))
                <= radius * radius;
    }

    private static List<Champion> getChampionsInRange(Square source, double radius, boolean hasVisibility) {
        List<Champion> champions = new ArrayList<>();
//        for (Player player : getPlayers()) {
//            for (Champion champion : player.getChampionsInArena()) {
//                if (inRange(source, champion.getSquare(), radius))
//                    champions.add(champion);
//            }
//        }
        getPlayers().forEach(player -> player.getChampionsInArena().forEach(champion -> {
            double finalRadius = !hasVisibility ? radius : (champion.getAttributes().isVisible() ? radius : radius / 2);
            if (inRange(source, champion.getSquare(), finalRadius))
                champions.add(champion);
        }));
        return champions;
    }

    private static List<Champion> getChampionsInRange(Square source, double radius) {
        return getChampionsInRange(source, radius, false);
    }

    private static int getTeamId(int playerId){
        return getPlayerById(playerId).getTeamId();
    }

    public static boolean isSameClass(Champion champion1, Champion champion2) {
        for (ChampionClass championClass1 : champion1.getChampionClasses()) {
            for (ChampionClass championClass2 : champion2.getChampionClasses()) {
                if (championClass1 == championClass2)
                    return true;
            }
        }
        return false;
    }

    public static boolean isSameClass(Champion champion, ChampionClass championClass) {
        for (ChampionClass championClass1 : champion.getChampionClasses()) {
            if (championClass == championClass1)
                return true;
        }
        return false;
    }

    public static List<Champion> getAlliesInRange(Square source, double radius, int playerId) {
        List<Champion> champions = getChampionsInRange(source, radius);
        int teamId = getTeamId(playerId);
        champions.removeIf(champion -> champion.getTeamId() != teamId);
        return champions;
    }

    public static List<Champion> getEnemiesInRange(Square source, double radius, int playerId) {
        List<Champion> champions = getChampionsInRange(source, radius, true);
        int teamId = getTeamId(playerId);
        champions.removeIf(champion -> champion.getTeamId() == teamId);
        return champions;
    }

    public static List<Champion> getEnemiesBehindChampion(Square square, int playerId) {
        List<Champion> champions = new ArrayList<>();
        for (Player player : getPlayers()) {
            int teamId = getTeamId(playerId);
            if (player.getTeamId() == teamId)
                continue;
            for (Champion champion : player.getChampionsInArena()) {
                if (champion.getSquare().getX() <= square.getX() && champion.getSquare().getY() <= square.getY())
                    champions.add(champion);
            }
        }
        return champions;
    }

    public static List<Champion> getChampionsOfClass(int playerId, ChampionClass championClass) {
        List<Champion> champions = new ArrayList<>();
        for (Player player : getPlayers()) {
            if (player.getId() == playerId) {
                for (Champion champion : player.getChampionsInArena()) {
                    if (isSameClass(champion, championClass))
                        champions.add(champion);
                }
                break;
            }
        }
        return champions;
    }


    public static List<Champion> getAllEnemies(int playerId) {
        List<Champion> champions = new ArrayList<>();
        int teamId = getTeamId(playerId);
        getPlayers().stream().filter(player -> player.getTeamId() != teamId)
                .forEach(player -> champions.addAll(player.getChampionsInArena()));
        return champions;
    }

    public static List<Champion> getAllAllies(int playerId) {
        List<Champion> champions = new ArrayList<>();
        int teamId = getTeamId(playerId);
        getPlayers().stream().filter(player -> player.getTeamId() == teamId)
                .forEach(player -> champions.addAll(player.getChampionsInArena()));
        return champions;
    }

    private static double getDistance(Square square1, Square square2) {
        return Math.sqrt(
                (square1.getX() - square2.getX()) * (square1.getX() - square2.getX()) +
                        (square1.getY() - square2.getY()) * (square1.getY() - square2.getY())
        );
    }

    public static double getNearestAllieRadius(Square square, int playerId, int championId) {
        double min = Double.MAX_VALUE;
        for (Champion champion : getAllAllies(playerId)) {
            if (champion.getId() != championId) {
                min = Math.min(min, getDistance(champion.getSquare(), square));
            }
        }
        return min;
    }

    public static Champion getNearestAllie(Square square, int playerId, int championId) {
        Champion selectedChampion = null;
        double min = Double.MAX_VALUE;
        for (Champion champion : getAllAllies(playerId)) {
            if (champion.getId() != championId) {
                double distance = getDistance(square, champion.getSquare());
                if (min > distance) {
                    min = distance;
                    selectedChampion = champion;
                }
            }
        }
        return selectedChampion;
    }

    public static Champion getNearestEnemy(Square square, double radius , int playerId){
        Champion selectedChampion = null;
        double min = Double.MAX_VALUE;
        for (Champion champion : getEnemiesInRange(square , radius , playerId)) {
                double distance = getDistance(square, champion.getSquare());
                if (min > distance) {
                    min = distance;
                    selectedChampion = champion;
                }
        }
        return selectedChampion;
    }

    public static List<Champion> getEnemiesInALine(Square square, int playerId) {
        List<Champion> champions = getAllEnemies(playerId).stream().filter(champion -> {
            return square.getX() == champion.getSquare().getX();
        }).collect(Collectors.toList());
        return champions;
    }

    public static List<Champion> getEnemiesInALine(Square square, int playerId, int bound) {
        List<Champion> champions = getAllEnemies(playerId).stream().filter(champion -> {
            return square.getX() == champion.getSquare().getX()
                    && Math.abs(square.getX() - champion.getSquare().getX()) <= bound;
        }).collect(Collectors.toList());
        return champions;
    }


    public static Champion getFurthestEnemy(Square square, int playerId) {
        Champion selectedChampion = null;
        double max = Double.MIN_VALUE;
        for (Champion champion : getAllEnemies(playerId)) {
            double distance = getDistance(square, champion.getSquare());
            if (max < distance) {
                max = distance;
                selectedChampion = champion;
            }
        }
        return selectedChampion;
    }

    public static int increaseByPercent(int value, int percent) {
        return value + value * percent / 100;
    }

    public static Player getPlayerById(int playerId) {
        for (Player player: getPlayers()) {
            if (player.getId() == playerId)
                return player;
        }
        return null;
    }


    public static Square getOppositeSquare(Square square){
        return new Square(AppInjector.getConfig().getArenaSize() - square.getX() , AppInjector.getConfig().getArenaSize() - square.getY());
    }

    public static void printAllPlayers() {
        getPlayers().forEach(player -> {
            player.getChampionsInArena().forEach(System.out::println);
        });
    }

    public static Direction getDirection(Square source, Square destination) {
        if (source.getX() == destination.getX()) {
            if (source.getY() < destination.getY()) {
                return Direction.Right;
            } else {
                return Direction.Left;
            }
        } else {
            if (source.getX() < destination.getX()) {
                return Direction.Down;
            } else {
                return Direction.Top;
            }
        }
    }

}
