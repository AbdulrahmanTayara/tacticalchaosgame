package gui.component;

import champion.Champion;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;

public class DetailsTooltip {
    private Champion champion;
    private boolean isOpened;
    private Tooltip tooltip;

    public DetailsTooltip(Champion champion) {
        this.tooltip = new Tooltip();
        this.champion = champion;
        this.isOpened = false;
        VBox vBox = new VBox();
        Label closeBtn = new Label("close");
        closeBtn.setOnMousePressed(e -> {
            this.tooltip.hide();
            this.isOpened = false;
        });
        vBox.getChildren().addAll(
                closeBtn,
                new Label("Name: " + champion.getName()),
                new Label("Cost: " + champion.getCost()),
                new Label("Level: " + champion.getAttributes().getLevel()),
                new Label("Health: " + champion.getAttributes().getHealth()),
                new Label("Armor: " + champion.getAttributes().getArmor()),
                new Label("Magic Resist: " + champion.getAttributes().getMagicResist()),
                new Label("Vision Range: " + champion.getAttributes().getVisionRange()),
                new Label("Attack Range: " + champion.getAttributes().getAttackRange()),
                new Label("Attack Damage: " + champion.getAttributes().getAttackDamage()),
                new Label("Movement Speed: " + champion.getAttributes().getMovementSpeed()),
                new Label("Critical Strike Chance: " + champion.getAttributes().getCriticalStrikeChance()),
                new Label("Critical Strike Damage: " + champion.getAttributes().getCriticalStrikeDamage()),
                new Label("Mana Start: " + champion.getAttributes().getManaStart()),
                new Label("Mana Cost: " + champion.getAttributes().getManaCost()),
                new Label("Classes: " + champion.getChampionClasses())
        );
        vBox.setStyle("-fx-font-size: 16px");
        closeBtn.setStyle("-fx-font-size: 16px; -fx-cursor: hand; -fx-underline: true");
        this.tooltip.setGraphic(vBox);
    }

    public void toggleTooltip(Node node, double x, double y) {
        if (this.isOpened) {
            this.tooltip.hide();
            this.isOpened = false;
        } else {
            this.tooltip.show(node, x, y);
            this.isOpened = true;
        }
    }


}