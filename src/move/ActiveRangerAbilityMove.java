package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

public class ActiveRangerAbilityMove extends Move{

    private static int abilityDamage = 250;
    Champion attacker;

    public ActiveRangerAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Champion target = Util.getFurthestEnemy(attacker.getSquare() , attacker.getPlayerId());
        Attributes attributes = new Attributes();
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
