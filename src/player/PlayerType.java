package player;

public enum PlayerType {
    CONSOLE,
    GUI,
    BOT;
}
