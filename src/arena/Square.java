package arena;

import injector.AppInjector;
import move.Direction;

public class Square {
    private int x;
    private int y;

    public Square(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = Math.max(0 , Math.min(x , AppInjector.getConfig().getArenaSize() - 1));
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = Math.max(0 , Math.min(y , AppInjector.getConfig().getArenaSize() - 1));
    }

    public Square move(Direction direction) {
        return new Square(x += direction.getX(), y += direction.getY());
    }

    @Override
    public String toString() {
        return x + ", " + y;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Square))
            return false;
        return x == ((Square) obj).getX() && y == ((Square) obj).getY();
    }

    @Override
    protected Square clone() {
        return new Square(x, y);
    }
}

