package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class GunslingerDamageCalculator {
    public static void Calculate(List<Champion> gunslingers) {
        Attributes attributes = new Attributes();
        attributes.setHitExtraTergetschance(40);
        if (gunslingers.size() >= 2 && gunslingers.size() < 4) {
            for (Champion champion : gunslingers) {
                attributes.setNumberOfExtraTergets(1);
                champion.addToAttributes(attributes);
            }
        } else if (gunslingers.size() >= 4 && gunslingers.size() < 6) {
            for (Champion champion : gunslingers) {
                attributes.setNumberOfExtraTergets(2);
                champion.addToAttributes(attributes);
            }
        } else if (gunslingers.size() >= 6) {
            for (Champion champion : gunslingers) {
                attributes.setNumberOfExtraTergets(4);
                champion.addToAttributes(attributes);
            }
        }
    }
}
