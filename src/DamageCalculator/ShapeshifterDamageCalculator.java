package DamageCalculator;

import champion.Attributes;
import champion.Champion;
import player.Player;

import java.util.List;

public class ShapeshifterDamageCalculator {
    public static void Calculate(List<Champion> shapeshifters) {
        Attributes attributes = new Attributes();
        if(shapeshifters.size()>=3 && shapeshifters.size()<6){
            attributes.setExtraHealthOnTransformation(40);
            for(Champion champion : shapeshifters)
                champion.addToAttributes(attributes);
        }
        else if(shapeshifters.size()>=6){
            attributes.setExtraHealthOnTransformation(100);
            for(Champion champion : shapeshifters)
                champion.addToAttributes(attributes);
        }
    }
}
