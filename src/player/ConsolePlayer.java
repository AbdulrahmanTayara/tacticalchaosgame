package player;

import arena.Square;
import champion.Champion;
import champion.AbilityType;
import injector.AppInjector;
import move.*;
import util.Config;
import util.InputUtil;
import util.Util;

import java.util.ArrayList;
import java.util.List;

public class ConsolePlayer extends Player {

    // options
    private final int BUY_FROM_STORE = 1;
    private final int SELL_CHAMPION = 5;
    private final int SWAP_FROM_BENCH_TO_ARENA = 2;
    private final int SWAP_FROM_ARENA_TO_BENCH = 3;
    private final int NONE = 10;
    private final int GIVE_ORDER_TO_CHAMPION = 4;
    private Config config = AppInjector.getConfig();


    private List<Champion> storeChampions;

    public ConsolePlayer() {
        super(PlayerType.CONSOLE.name());
    }

    @Override
    public void startBuyingRound() {
        storeChampions = presenter.getChampionsFromStore();
        outer:
        while (true) {
            System.out.println("Player : " + getId());
            int playerChoice = propagateOptionsInBuyingRound();
            switch (playerChoice) {
                case BUY_FROM_STORE:
                    pickChampionToBuy(storeChampions);
                    break;
                case SWAP_FROM_ARENA_TO_BENCH:
                    swapFromArenaToBench();
                    break;
                case SWAP_FROM_BENCH_TO_ARENA:
                    swapFromBenchToArena();
                    break;
                case NONE:
                    break outer;
                default:
                    break;
            }
        }
        presenter.playerFinishedTheRound();
    }

    @Override
    public void startPlanningRound() {
        storeChampions = presenter.getChampionsFromStore();
        outer:
        while (true) {
            System.out.println("Player : " + getId());
            int playerChoice = propagateOptionsInPlanningRound();
            switch (playerChoice) {
                case BUY_FROM_STORE:
                    pickChampionToBuy(storeChampions);
                    break;
                case SELL_CHAMPION:
                    pickChampionToSell();
                    break;
                case SWAP_FROM_ARENA_TO_BENCH:
                    swapFromArenaToBench();
                    break;
                case SWAP_FROM_BENCH_TO_ARENA:
                    swapFromBenchToArena();
                    break;
                case GIVE_ORDER_TO_CHAMPION:
                    giveOrderToChampion();
                    break;
                case NONE:
                    break outer;
                default:
                    break;
            }
        }
        presenter.playerFinishedTheRound();
    }

    private int propagateOptionsInBuyingRound() {
        System.out.println("Choose from a list :");
        System.out.println(
                "1) Buy champion from store : \n"
                        + "2) Move champion from bench to the arena : \n"
                        + "3) Move champion from arena to the bench : \n"
                        + "10) Stop buying round"
        );

        return InputUtil.getInt();
    }

    private int propagateOptionsInPlanningRound() {
        System.out.println("Choose order from a list :");
        System.out.println(
                "1) Buy champion from store : \n"
                        + "2) Move champion from bench to the arena : \n"
                        + "3) Move champion from arena to the bench : \n"
                        + "4) Give an order to a champion in arena : \n"
                        + "5) Sell champion : \n"
                        + "10) Stop planning round"
        );

        return InputUtil.getInt();
    }

    private void giveOrderToChampion() {
        System.out.println("Choose champion to give it an order");
        printChampions(getChampionsInArena());
        int index = InputUtil.getInt() - 1;
        if (index >= getChampionsInArena().size())
            return;
        Champion champion = getChampionsInArena().get(index);

        if (champion.getAttributes().getStunDuration() > 0) {
            System.out.println("This champion is stunned, Try again");
            giveOrderToChampion();
        } else if (champion.getMoves().size() >= AppInjector.getConfig().getMaxOrdersEachRound()) {
            System.out.println("This champion has already received all orders");
            return;
        }
        printOrdersToChampion(champion);
    }

    private void printOrdersToChampion(Champion champion) {
        List<Champion> targets = Util.getEnemiesInRange(champion.getSquare(), champion.getAttributes().getAttackRange(), champion.getPlayerId());
        int i, numberOfTargets = targets.size();
        for (i = 0; i < numberOfTargets; i++) {
            Champion target = targets.get(i);
            System.out.println((i + 1) + ") attack : " + target);
        }
        System.out.println((++i) + ") " + "Move to the left");
        System.out.println((++i) + ") " + "Move to the right");
        System.out.println((++i) + ") " + "Move up");
        System.out.println((++i) + ") " + "Move down");
        System.out.println((++i) + ") " + "Activate Ability");
        System.out.println((++i) + ") " + "Return Back");
        int index = InputUtil.getInt();
        if (index < 0 || index > (numberOfTargets + 6)) {
            System.out.println("Wrong choice");
            printOrdersToChampion(champion);
        } else if (index == (numberOfTargets + 6))
            return;
        else if (index == numberOfTargets + 1) {  //Move to the left order
            champion.addMove(new WalkMove(champion, Direction.Left));
        } else if (index == numberOfTargets + 2) {     //Move to the right order
            champion.addMove(new WalkMove(champion, Direction.Right));
        } else if (index == numberOfTargets + 3) {
            champion.addMove(new WalkMove(champion, Direction.Top));
        } else if (index == numberOfTargets + 4) {
            champion.addMove(new WalkMove(champion, Direction.Down));
        } else if (index == numberOfTargets + 5) {
            if (champion.getAttributes().getMana() > champion.getAttributes().getManaCost()) {
                System.out.println("No enough mana");
                return;
            }
            if (champion.getAbilityType() == AbilityType.TARGET) {
                if (targets.size() == 0) {
                    System.out.println("No enemies in attack range, try again");
                    return;
                }
                printChampions(targets);
                int targetIndex = InputUtil.getInt() - 1;
                if (targetIndex >= targets.size()) return;
                champion.addMove(MoveFactory.getChampionAbility(champion, targets.get(targetIndex)));
                champion.getAttributes().setMana((int)(champion.getAttributes().getMana() - champion.getAttributes().getManaCost()));
                return;
            }
            champion.addMove(MoveFactory.getChampionAbility(champion, null));

        } else { // basic attack
            if (targets.size() == 0) {
                System.out.println("No enemies in attack range, try again");
                printOrdersToChampion(champion);
            } else {
                champion.addMove(MoveFactory.getBasicAttack(champion, targets.get(index - 1)));
            }
        }

    }

    private void swapFromArenaToBench() {
        if (getNumberOfPermissibleSwaps() <= 0) {
            System.out.println("You have exceeded the number of allowed swaps");
            return;
        }
        if (getChampionsInBench().size() >= config.getPlayerChampionsInBench()) {
            System.out.println("Bench is full");
            return;
        } else if (getChampionsInArena().isEmpty()) {
            System.out.println("Arena is empty");
            return;
        }
        System.out.println("Choose champion to move it to the bench");
        printChampions(getChampionsInArena());
        int index = InputUtil.getInt() - 1;
        if (index == getChampionsInArena().size())
            return;
        Champion champion = getChampionsInArena().get(index);
        swapFromArenaToBench(champion);
        setNumberOfPermissibleSwaps(getNumberOfPermissibleSwaps() - 1);
        System.out.println("Champion has been moved");
    }

    private void swapFromBenchToArena() {
        if (getNumberOfPermissibleSwaps() <= 0) {
            System.out.println("You have exceeded the number of allowed swaps");
            return;
        }
        if (getChampionsInArena().size() >= getNumberOfChampionInArena()) {
            System.out.println("Arena is full");
            return;
        } else if (getChampionsInBench().isEmpty()) {
            System.out.println("Bench is empty");
            return;
        }
        System.out.println("Choose champion to move it to the arena");
        printChampions(getChampionsInBench());
        int index = InputUtil.getInt() - 1;
        if (index == getChampionsInBench().size())
            return;
        System.out.println("Input champion's position in the arena (x then y) : ");
        int x = InputUtil.getInt();
        int y = InputUtil.getInt();
        x = x >= config.getArenaSize() ? config.getArenaSize() - 1 : x;
        y = y >= config.getArenaSize() ? config.getArenaSize() - 1 : y;
        Champion champion = getChampionsInBench().get(index);
        champion.setSquare(new Square(x, y));
        swapFromBenchToArena(champion);
        setNumberOfPermissibleSwaps(getNumberOfPermissibleSwaps() - 1);
        System.out.println("Champion has been moved");
    }

    private void printChampionsToBuy(List<Champion> champions) {
        int i;
        for (i = 0; i < champions.size(); i++) {
            Champion champion = champions.get(i);
            boolean recommended = isRecommended(champion) > 0;
            System.out.println(
                    (i + 1) + ") Name = " + champion.getName() + ", "
                            + "Cost = " + champion.getCost()
                            + (recommended ? "\u001B[32m *recommended* \u001B[0m" : "")
            );
        }
        System.out.println((i + 1) + ") Return Back");
    }

    private void printChampions(List<Champion> champions) {
        int i;
        for (i = 0; i < champions.size(); i++) {
            Champion champion = champions.get(i);
            System.out.println((i + 1) + ") " + champion);
        }
        System.out.println((i + 1) + ") " + "Return Back");
    }

    private void pickChampionToBuy(List<Champion> champions) {
        if (getCoins() <= 0) {
            System.out.println("You have no coins to buy a champion");
            return;
        }
        System.out.println("Your coins = " + getCoins());

        printChampionsToBuy(champions);

        int index = InputUtil.getInt() - 1;
        if (index > champions.size() || index < 0) {
            System.out.println("Wrong input, Try again");
            pickChampionToBuy(champions);
        } else if (index == champions.size())
            return;

        else {
            if (canBuy(champions.get(index).getCost())) {
                System.out.println("Champion added to the bench");
                buyChampion(presenter.buyChampionFromStore(index));
            } else {
                System.out.println("You can't buy this champion, check your coins and bench, Try again");
                pickChampionToBuy(champions);
            }
        }
    }

    private void pickChampionToSell() {
        List<Champion> champions = new ArrayList<>();
        champions.addAll(getChampionsInArena());
        champions.addAll(getChampionsInBench());
        if (champions.isEmpty()) {
            System.out.println("You don't have any champion");
            return;
        }

        System.out.println("Pick a champion to sell it");
        printChampions(champions);

        int index = InputUtil.getInt() - 1;
        if (index > champions.size() || index < 0) {
            System.out.println("Wrong input, Try again");
            pickChampionToSell();
        } else if (index == champions.size())
            return;

        else {
            sellChampion(champions.get(index));
            presenter.sellChampionFromStore(champions.get(index));
            System.out.println("Champion sold");
        }
    }

    @Override
    public void gameFinished(Team winner) {
        System.out.println("Game Finished");
        System.out.println("The winner team is : " + winner.getId());
    }

    @Override
    public void gameFinishedAsDraw() {
        System.out.println("Game Finished As Draw");
    }

    public void printPlayerChampions(List<Champion> champions){
        System.out.println("Player No." + getId());
        for(Champion champion : champions){
            System.out.println(champion);
        }
    }

    @Override
    public String getColor() {
        return "";
    }
}
