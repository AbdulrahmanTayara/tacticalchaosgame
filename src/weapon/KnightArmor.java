package weapon;

import champion.Attributes;
import champion.ChampionClass;

public class KnightArmor extends Weapon{

    public KnightArmor() {
        super(WeaponType.KNIGHT_ARMOR.name(), "Knight Armor");
        super.setChampionClass(ChampionClass.Knight);
    }

    @Override
    public Attributes getWeaponBuff() {
        Attributes attributes = new Attributes();
        attributes.setArmor(15);
        return attributes;
    }
}
