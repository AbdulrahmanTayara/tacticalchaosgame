package move;

import arena.Square;
import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveSolAbilityMove extends Move{
    Champion attacker;
    private int abilityDamage = 100;
    private double radius = 25;


    public ActiveSolAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getEnemiesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setAbilityType(AbilityType.ability);
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            Square square = target.getSquare();
            target.setSquare(new Square(square.getX() , square.getY() - 30));
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
