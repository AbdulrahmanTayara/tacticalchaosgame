package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveAsheAbilityMove extends Move{

    Champion attacker , target;
    private static int abilityDamage = 200;
    private static int stunDurationToChamps = 2;

    ActiveAsheAbilityMove(Champion attacker , Champion target){
        this.attacker = attacker;
        this.target = target;
    }
    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int movementSpeed = attacker.getAttributes().getMovementSpeed();
        attributes.setMovementSpeed((movementSpeed * 10)/100);
        attributes.setStunDuration(stunDurationToChamps);
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
