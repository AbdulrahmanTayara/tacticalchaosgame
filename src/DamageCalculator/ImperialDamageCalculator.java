package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class ImperialDamageCalculator {
    public static void Calculate(List<Champion> imperials) {
        Attributes attributes = new Attributes();
        if(imperials.size()>=1)
        for(Champion champion : imperials){
            attributes.setAttackDamage(champion.getAttributes().getAttackDamage());
            champion.addToAttributes(attributes);
        }
    }
}
