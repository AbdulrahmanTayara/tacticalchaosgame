package DamageCalculator;

import champion.Champion;
import champion.ChampionClass;
import player.Player;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DamageCalculatorFactory{
    private static final List<ChampionClass> classes = Arrays.asList(
            ChampionClass.Assassin,
            ChampionClass.Yordle,
            ChampionClass.Void,
            ChampionClass.Ranger,
            ChampionClass.Sorcerer,
            ChampionClass.Shapeshifter,
            ChampionClass.Pirate,
            ChampionClass.Noble,
            ChampionClass.Ninja,
            ChampionClass.BladeMaster,
            ChampionClass.Brawler,
            ChampionClass.Demon,
            ChampionClass.Dragon,
            ChampionClass.Elementalist,
            ChampionClass.Knight,
            ChampionClass.Imperial,
            ChampionClass.Gunslinger,
            ChampionClass.Glacial
    );


    public static void CalculateClasses(Player player) {
        List<Champion> champions = player.getChampionsInArena();
        List<Champion> championsFromSameClass = new ArrayList<>();
        for (ChampionClass championClass : classes) {
            championsFromSameClass.clear();

            for (Champion champion : champions) {
                for (ChampionClass currentClass : champion.getChampionClasses()) {
                    if (currentClass == championClass)
                        championsFromSameClass.add(champion);
                }
            }
            calculate(championsFromSameClass, championClass, player);
        }

    }

    private static void calculate(List<Champion> championsFromSameClass, ChampionClass championClass, Player player) {
//        if (championClass.name().equals("Assassin"))
//            //AssassinDamageCalculator.Calculate(championsFromSameClass);
        if (championClass.name().equals("Yordle"))
            YordleDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Void"))
            VoidDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Ranger"))
            RangerDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Sorcerer"))
             SorcererDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Shapeshifter"))
            ShapeshifterDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Pirate"))//send the player with the champions
            PirateDamageCalculator.Calculate(player, championsFromSameClass);
        else if (championClass.name().equals("Noble"))//send all champions in arena
            NobleDamageCalculator.Calculate(player.getChampionsInArena());
        else if (championClass.name().equals("Ninja"))
            NinjaDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("BladeMaster"))
            BladeMasterDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Brawler"))
            BrawlerDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Demon"))
            DemonDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Dragon"))
            DragonDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Elementalist"))//need the player + elementalists
            ElementalistDamageCalculator.Calculate(player, championsFromSameClass);
        else if (championClass.name().equals("Knight"))//need all champions in arena
            KnightDamageCalculator.Calculate(player.getChampionsInArena());
        else if (championClass.name().equals("Imperial"))
            ImperialDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Gunslinger"))
            GunslingerDamageCalculator.Calculate(championsFromSameClass);
        else if (championClass.name().equals("Glacial"))
            GlacialDamageCalculator.Calculate(championsFromSameClass);
    }


}

