package store;

import champion.Champion;
import champion.ChampionClass;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import injector.AppInjector;
import util.Config;
import util.Util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Store {

    protected List<Champion> championsList;
    protected int[] championsCount;
    protected List<Champion> roundChampionsList;
    private Config config = AppInjector.getConfig();


    private final int MAX_OF_CHAMPION_IN_STORE = config.getMaxChampionInStore();

    public Store() {

        //Get champions from json file
        championsList = new ArrayList<>();
        Gson gson = new Gson();
        FileReader fileReader;
        try {
            fileReader = new FileReader("Data/Champions.json");
            JsonReader jsonReader = new JsonReader(fileReader);
            JsonObject[] champions = gson.fromJson(jsonReader, JsonObject[].class);
            for (JsonObject champion : champions) {
                championsList.add(new Champion(champion));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        //init champions counters
        championsCount = new int[championsList.size() + 10];
        Arrays.fill(championsCount, MAX_OF_CHAMPION_IN_STORE);

        filter();
    }


    public void filter() {
        List<ChampionClass> acceptedClasses = config.getAcceptedClasses();
        championsList.removeIf(champion -> {
            for (ChampionClass championClass: acceptedClasses) {
                if (Util.isSameClass(champion, championClass))
                    return false;
            }
            return true;
        });
    }
    public Champion buy(int choice) {
        Champion champion = roundChampionsList.get(choice);
        this.championsCount[roundChampionsList.get(choice).getId()] -= 1;
        this.roundChampionsList.remove(roundChampionsList.get(choice));
        return champion.clone();
    }

    public void sell(Champion champion) {
        int level = champion.getAttributes().getLevel();
        int count = 1;
        for (int i = 1; i < level; i++) {
            count *= 3;
        }
        championsCount[champion.getId()] += count;
    }

    public abstract void generateChampions();

    public List<Champion> getRoundChampions() {
        return roundChampionsList;
    }
}
