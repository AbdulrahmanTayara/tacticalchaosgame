package store;

import champion.Champion;
import filter.Filter;
import filter.IFilterable;

import java.util.ArrayList;
import java.util.List;

public class TestStore extends Store implements IFilterable {


    private List<Champion> testChampionList;

    public TestStore() {
        super();
        this.testChampionList = new ArrayList<>();
    }

    @Override
    public void generateChampions() {
        this.testChampionList.addAll(this.championsList);
    }

    @Override
    public List<Champion> getRoundChampions() {
        return this.testChampionList;
    }

    @Override
    public void addFilter(Filter filterObject) {
        this.testChampionList.removeIf(champion -> {
            return !filterObject.isAccepted(champion);
        });
    }
}
