package filesmanager;

import adapters.RunTimeAdapters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import game.GameData;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileManager implements IFileManger {


    private SimpleDateFormat simpleDateFormat;
    private BufferedWriter writer;

    private JsonArray recordingStates = new JsonArray();

    public FileManager() {
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-SS");
    }

    private String RECORD_FILE_NAME = "Data\\replay\\replay-game.json";

    @Override
    public void saveGame(GameData gameData) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        new Thread(() -> {
            String json = gson.toJson(gameData);
            String fileName = simpleDateFormat.format(new Date()) + ".json";
            saveOnFile(json, "Data\\saved_game\\" + fileName);

        }).start();
    }

    private void saveOnFile(String json, String path) {
        try {
            writer = new BufferedWriter(new FileWriter(path));
            writer.write(json);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public GameData loadGame(String path) {
        Gson gson = initGson();
        try {
            return gson.fromJson(new FileReader(path), GameData.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Gson initGson() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(RunTimeAdapters.getPlayersAdapter())
                .registerTypeAdapterFactory(RunTimeAdapters.getRoundsAdapter())
                .registerTypeAdapterFactory(RunTimeAdapters.getWeaponsAdapter())
                .create();
    }

    @Override
    public void addRecodingState(GameData gameData) {
//        recordingStates.add(gameData);
        Gson gson = new Gson();
        recordingStates.add(gson.toJsonTree(gameData));
    }

    @Override
    public void saveRecordingStats() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(recordingStates);
        saveOnFile(json, RECORD_FILE_NAME);
    }

    @Override
    public List<GameData> getRecordingStates() {
        Gson gson = initGson();
        List<GameData> history = new ArrayList<>();
        try {
            history = gson.fromJson(new FileReader(RECORD_FILE_NAME),
                    new TypeToken<List<GameData>>(){}.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return history;
    }
}

