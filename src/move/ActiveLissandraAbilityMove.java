package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

public class ActiveLissandraAbilityMove extends Move{
    private static int abilityDamage = 550;
    Champion attacker , target;

    public ActiveLissandraAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setHealth(-abilityDamage - attacker.getAttributes().getAbilityPower() * abilityDamage/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        attributes = new Attributes();
        Champion targetAllie = Util.getNearestAllie(target.getSquare() , target.getPlayerId() , target.getId());
        attributes.setHealth(-abilityDamage - attacker.getAttributes().getAbilityPower() * abilityDamage/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        if (targetAllie != null)
            targetAllie.acceptDamage(attributes);
    }
}
