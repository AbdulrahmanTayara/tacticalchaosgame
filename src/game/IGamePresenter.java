package game;

import champion.Champion;
import player.Player;

import java.util.List;

public interface IGamePresenter {
    List<Champion> getChampionsFromStore();
    void playerFinishedTheRound();
    Champion buyChampionFromStore(int index);
    void sellChampionFromStore(Champion champion);
    List<Player> getPlayers();
    int getCurrentRound();
    void saveGame();
    List<String> getScoreBoard ();
    void startRecording();
}
