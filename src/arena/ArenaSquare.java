package arena;

import champion.Champion;
import weapon.Weapon;

import java.util.ArrayList;
import java.util.List;

public class ArenaSquare {
    private transient List<Champion> champions = new ArrayList<>();
    private List<Weapon> weapons = new ArrayList<>();
    private SquareType squareType;


    public ArenaSquare() {
        squareType = SquareType.Standard;
    }

    public List<Champion> getChampions() {
        return champions;
    }

    public void setChampions(List<Champion> champions) {
        this.champions = champions;
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<Weapon> weapons) {
        this.weapons = weapons;
    }

    public SquareType getSquareType() {
        return squareType;
    }

    public void setSquareType(SquareType squareType) {
        this.squareType = squareType;
    }

    public void addChampion(Champion champion) {
        champions.add(champion);
    }

    public void removeChampion(Champion champion) {
        champions.remove(champion);
    }

    public void addWeapon(Weapon weapon) {
        weapons.add(weapon);
    }
}
