package round;

public enum RoundType {
    BUYING,
    PLANNING,
    EXECUTING;
}
