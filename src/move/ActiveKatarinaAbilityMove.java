package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;

public class ActiveKatarinaAbilityMove extends Move{
    Champion attacker , target;

    public ActiveKatarinaAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        int targetMana = target.getAttributes().getMana();
        Attributes attributes = new Attributes();
        attributes.setMana(-targetMana);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
