package adapters;

import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import player.*;
import round.*;
import weapon.*;

public class RunTimeAdapters {

    public static RuntimeTypeAdapterFactory<Player> getPlayersAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Player.class, "playerType")
                .registerSubtype(GuiPlayer.class, PlayerType.GUI.name())
                .registerSubtype(ConsolePlayer.class, PlayerType.CONSOLE.name())
                .registerSubtype(AutoPlayer.class, PlayerType.BOT.name());
    }

    public static RuntimeTypeAdapterFactory<Round> getRoundsAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Round.class, "roundType")
                .registerSubtype(Buying.class, RoundType.BUYING.name())
                .registerSubtype(Executing.class, RoundType.EXECUTING.name())
                .registerSubtype(Planning.class, RoundType.PLANNING.name());
    }

    public static RuntimeTypeAdapterFactory<Weapon> getWeaponsAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Weapon.class, "weaponType")
                .registerSubtype(AngryCloak.class, WeaponType.ANGRY_CLOAK.name())
                .registerSubtype(KnightArmor.class, WeaponType.KNIGHT_ARMOR.name())
                .registerSubtype(MagicHat.class, WeaponType.MAGIC_HAT.name())
                .registerSubtype(NightShift.class, WeaponType.NIGHT_SHIFT.name())
                .registerSubtype(UniverseCore.class, WeaponType.UNIVERSE_CORE.name())
                .registerSubtype(VoidHit.class, WeaponType.VOID_HIT.name())
                .registerSubtype(WarriorGloves.class, WeaponType.WARRIOR_GLOVES.name());
    }

}
