package DamageCalculator;

import arena.Arena;
import arena.Square;
import champion.Champion;
import injector.AppInjector;
import player.Player;
import util.Util;

import java.util.List;

public class AssassinDamageCalculator  {
    public static void Calculate(List<Champion> assassins) {
        if(assassins.size()>=2 && assassins.size() <4)
            for(Champion champion : assassins){
                champion.getAttributes().setAttackDamage(champion.getAttributes().getAttackDamage()+30);
                Square square = champion.getSquare();
                champion.setSquare(new Square(square.getX() , AppInjector.getConfig().getArenaSize() - square.getY() - 1));
            }

        else if(assassins.size()>=4 )
            for(Champion champion : assassins){
                champion.getAttributes().setAttackDamage(champion.getAttributes().getAttackDamage()+60);
                Square square = champion.getSquare();
                champion.setSquare(new Square(square.getX() , AppInjector.getConfig().getArenaSize()-square.getY() - 1));
            }

    }
}
