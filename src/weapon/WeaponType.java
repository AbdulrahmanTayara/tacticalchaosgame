package weapon;

public enum WeaponType {
    ANGRY_CLOAK,
    KNIGHT_ARMOR,
    MAGIC_HAT,
    NIGHT_SHIFT,
    UNIVERSE_CORE,
    VOID_HIT,
    WARRIOR_GLOVES;
}
