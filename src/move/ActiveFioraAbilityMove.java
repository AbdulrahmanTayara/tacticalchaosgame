package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveFioraAbilityMove extends Move{
    Champion target;
    Champion attacker;

    public ActiveFioraAbilityMove(Champion attacker , Champion target) {
        this.target = target;
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setStunDuration(1);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        attributes = new Attributes();
        attributes.setImmunityDuration(1);
        attacker.addToAttributes(attributes);
    }
}
