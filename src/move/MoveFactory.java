package move;

import champion.Champion;

import java.util.ArrayList;
import java.util.List;

public class MoveFactory {
    public static List<ChampionMoves> getMoves(Champion champion) {
        List<ChampionMoves> moves = new ArrayList<ChampionMoves>();
        if (champion.getAttributes().getStunDuration() == 0) {
            moves.add(ChampionMoves.WALK);
            moves.add(ChampionMoves.BASIC_ATTACK);
            moves.add(ChampionMoves.ABILITY);
        }
        return moves;
    }

    public static Move getBasicAttack(Champion attacker, Champion target) {
        return new BasicAttackMove(attacker, target);
    }

    public static Move getChampionAbility(Champion champion, Champion targetChampion) {
        if (champion.getName().equals("Aatrox"))
            return new ActiveAatroxAbilityMove(champion);
        else if (champion.getName().equals("Ahri"))
            return new ActiveAhriAbilityMove(champion);
        else if (champion.getName().equals("Akali"))
            return new ActiveAkaliAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Anivia"))
            return new ActiveAniviaAbilityMove(champion);
        else if (champion.getName().equals("Ashe"))
            return new ActiveAsheAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Brand"))
            return new ActiveBrandAbilityMove(champion);
        else if (champion.getName().equals("Chogath"))
            return new ActiveChogathAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Darius"))
            return new ActiveDariusAbilityMove(champion);
        else if (champion.getName().equals("Draven"))
            return new ActiveDravenAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Evelynn"))
            return new ActiveEvelynnAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Fiora"))
            return new ActiveFioraAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Gankplank"))
            return new ActiveGankplankAbilityMove(champion);
        else if (champion.getName().equals("Garen"))
            return new ActiveGarenAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Gnar"))
            return new ActiveGnarAbilityMove(champion);
        else if (champion.getName().equals("Graves"))
            return new ActiveGravesAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Karthus"))
            return new ActiveKarthusAbilityMove(champion);
        else if (champion.getName().equals("Kassadin"))
            return new ActiveKassadinAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Katarina"))
            return new ActiveKatarinaAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Kayle"))
            return new ActiveKayleAbilityMove(champion);
        else if (champion.getName().equals("Kennen"))
            return new ActiveKennenAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Khazix"))
            return new ActiveKhazixAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Kindred"))
            return new ActiveKindredAbilityMove(champion);
        else if (champion.getName().equals("Leona"))
            return new ActiveLeonaAbilityMove(champion);
        else if (champion.getName().equals("Lissandra"))
            return new ActiveLissandraAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Lucian"))
            return new ActiveLucianAbilityMove(champion);
        else if (champion.getName().equals("Lulu"))
            return new ActiveLuluAbilityMove(champion);
        else if (champion.getName().equals("MissFortune"))
            return new ActiveMissFortuneAbilityMove(champion, targetChampion);
        else if (champion.getName().equals("Mordekaiser"))
            return new ActiveMordekaiserAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Morgana"))
            return new ActiveMorganaAbilityMove(champion);
        else if (champion.getName().equals("Nidale"))
            return new ActiveNidaleAbilityMove(champion);
        else if (champion.getName().equals("Poppy"))
            return new ActivePoppyAbilityMove(champion);
        else if (champion.getName().equals("Pyke"))
            return new ActivePykeAbilityMove(champion);
        else if (champion.getName().equals("Ranger"))
            return new ActiveRangerAbilityMove(champion);
        else if (champion.getName().equals("Shen"))
            return new ActiveShenAbilityMove(champion);
        else if (champion.getName().equals("Shyvana"))
            return new ActiveShyvanaAbilityMove(champion);
        else if (champion.getName().equals("Sol"))
            return new ActiveSolAbilityMove(champion);
        else if(champion.getName().equals("Swain"))
            return new ActiveSwainAbilityMove(champion);
        else if(champion.getName().equals("Talon"))
            return new ActiveTalonAbilityMove(champion);
        else if(champion.getName().equals("Varus"))
            return new ActiveVarusAbilityMove(champion);
        else if (champion.getName().equals("Vayne"))
            return new ActiveVayneAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Veiger"))
            return new ActiveVeigerAbilityMove(champion , targetChampion);
        else if (champion.getName().equals("Volibear"))
            return new ActiveVolibearAbilityMove(champion);
        else if (champion.getName().equals("Warwick"))
            return new ActiveWarWickAbilityDamage(champion, targetChampion);
        else if(champion.getName().equals("Yasuo"))
            return new ActiveYasuoAbilityMove(champion);
        else if(champion.getName().equals("Zed"))
            return new ActiveZedAbilityMove(champion);
        else if(champion.getName().equals("Azir"))
            return new ActiveAzirAbilityMove(champion);
        return null;
    }

}
