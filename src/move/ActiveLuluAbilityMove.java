package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;
import java.util.Random;

public class ActiveLuluAbilityMove extends Move{

    private static int Healing = 150;
    Champion attacker;

    public ActiveLuluAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setHealth(Healing);
        attacker.addToAttributes(attributes);
        List<Champion> list = Util.getAllAllies(attacker.getPlayerId());
        list.remove(attacker);
        int championsToProtect = 2;
        while(championsToProtect > 0 && list.size() > 0){
            attributes = new Attributes();
            championsToProtect--;
            Random random = new Random();
            int randomNumber = random.nextInt(list.size() - 1);
            Champion temp = list.get(randomNumber);
            attributes.setHealth(Healing);
            temp.addToAttributes(attributes);
            list.remove(randomNumber);
        }
    }
}
