package injector;

import arena.Arena;
import arenaobservable.ArenaObserver;
import com.google.gson.Gson;
import game.Game;
import game.GameData;
import gui.GameFrame;
import javafx.application.Application;
import store.RandomStore;
import store.Store;
import util.Config;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class AppInjector {

    private static Store store;
    private static Game game;
    private static Config config;
    private static Arena arena;

    public static Store getStore() {
        if (store == null)
            store = new RandomStore();
        return store;
    }

    public static Game getGame() {
        if (game == null)
            game = new Game();
        return game;
    }

    public static void setGame(GameData gameData) {
        game = new Game(gameData);
    }

    public static Config getConfig() {
        if (config == null) {
            try {
                config = new Gson().fromJson(new FileReader("Data/Config.json"), Config.class);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return config;
    }

    public static void setConfig(Config config) {
        AppInjector.config = config;
    }

    public static Arena getArena() {
        if (arena == null) {
            arena = new Arena(config.getArenaSize());
        }
        return arena;
    }

    public static void setArena(Arena arena) {
        AppInjector.arena = arena;
    }

    public static ArenaObserver getArenaObserver() {
        return getArena();
    }
}
