package move;

import champion.Attributes;
import champion.Champion;

public class ActiveGankplankAbilityMove extends Move{
    Champion attacker;

    public ActiveGankplankAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int attackRange = attacker.getAttributes().getAttackRange();
        int attackDamage = attacker.getAttributes().getAttackDamage();
        attributes.setAttackRange(attackRange);
        attributes.setAttackDamage(attackDamage);
        attacker.addToAttributes(attributes);
    }
}
