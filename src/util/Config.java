package util;

import champion.ChampionClass;
import com.google.gson.annotations.SerializedName;
import game.GameMode;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Config {
    @SerializedName("ArenaSize")
    private int arenaSize;
    @SerializedName("BuyingRoundsNumber")
    private int buyingRoundsNumber;
    @SerializedName("PlayerChampionsInArena")
    private int playerChampionsInArena;
    @SerializedName("PlayerChampionsInBench")
    private int playerChampionsInBench;
    @SerializedName("RandomStoreListSize")
    private int randomStoreListSize;
    @SerializedName("MaxChampionInStore")
    private int maxChampionInStore;
    @SerializedName("NumberOfCoinsEachRound")
    private int numberOfCoinsEachRound;
    @SerializedName("ChampionMaxLevel")
    private int championMaxLevel;
    @SerializedName("GameMode")
    private String gameMode;
    @SerializedName("MaxSwaps")
    private int maxSwaps;
    @SerializedName("AcceptedClasses")
    private List<String> acceptedClasses;
    @SerializedName("MaxRounds")
    private int maxRounds;
    @SerializedName("MaxOrdersEachRound")
    private int maxOrdersEachRound;
    @SerializedName("NumberOfPlayers")
    private int numberOfPlayers;
    @SerializedName("NumberOfBots")
    private int numberOfBots;
    @SerializedName("Theme")
    private String theme;

    private transient boolean replyMode = false;
    private transient boolean recording = false;

    public Config(int arenaSize, int buyingRoundsNumber, int playerChampionsInArena, int playerChampionsInBench, int randomStoreListSize, int maxChampionInStore, int numberOfCoinsEachRound, int championMaxLevel, int maxSwaps, List<String> acceptedClasses, int maxRounds, int maxOrdersEachRound, int numberOfPlayers, int numberOfBots, String theme) {
        this.arenaSize = arenaSize;
        this.buyingRoundsNumber = buyingRoundsNumber;
        this.playerChampionsInArena = playerChampionsInArena;
        this.playerChampionsInBench = playerChampionsInBench;
        this.randomStoreListSize = randomStoreListSize;
        this.maxChampionInStore = maxChampionInStore;
        this.numberOfCoinsEachRound = numberOfCoinsEachRound;
        this.championMaxLevel = championMaxLevel;
        this.maxSwaps = maxSwaps;
        this.acceptedClasses = acceptedClasses;
        this.maxRounds = (maxRounds - buyingRoundsNumber) * 2;
        this.maxOrdersEachRound = maxOrdersEachRound;
        this.numberOfPlayers = numberOfPlayers;
        this.numberOfBots = numberOfBots;
        this.theme = theme;
        replyMode = false;
        recording = false;
    }

    public int getArenaSize() {
        return arenaSize;
    }

    public int getBuyingRoundsNumber() {
        return buyingRoundsNumber;
    }

    public int getPlayerChampionsInArena() {
        return playerChampionsInArena;
    }

    public int getPlayerChampionsInBench() {
        return playerChampionsInBench;
    }

    public int getRandomStoreListSize() {
        return randomStoreListSize;
    }

    public int getMaxChampionInStore() {
        return maxChampionInStore;
    }

    public int getNumberOfCoinsEachRound() {
        return numberOfCoinsEachRound;
    }

    public void setArenaSize(int arenaSize) {
        this.arenaSize = arenaSize;
    }

    public void setBuyingRoundsNumber(int buyingRoundsNumber) {
        this.buyingRoundsNumber = buyingRoundsNumber;
    }

    public void setPlayerChampionsInArena(int playerChampionsInArena) {
        this.playerChampionsInArena = playerChampionsInArena;
    }

    public void setPlayerChampionsInBench(int playerChampionsInBench) {
        this.playerChampionsInBench = playerChampionsInBench;
    }

    public void setRandomStoreListSize(int randomStoreListSize) {
        this.randomStoreListSize = randomStoreListSize;
    }

    public void setMaxChampionInStore(int maxChampionInStore) {
        this.maxChampionInStore = maxChampionInStore;
    }

    public void setNumberOfCoinsEachRound(int numberOfCoinsEachRound) {
        this.numberOfCoinsEachRound = numberOfCoinsEachRound;
    }

    public int getChampionMaxLevel() {
        return championMaxLevel;
    }

    public void setChampionMaxLevel(int championMaxLevel) {
        this.championMaxLevel = championMaxLevel;
    }

    public GameMode getGameMode() {
        return GameMode.valueOf(gameMode.toUpperCase());
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public int getMaxSwaps() {
        return maxSwaps;
    }

    public void setMaxSwaps(int maxSwaps) {
        this.maxSwaps = maxSwaps;
    }

    public List<ChampionClass> getAcceptedClasses() {
        return acceptedClasses.stream().flatMap((Function<String, Stream<ChampionClass>>) s ->
                Stream.of(ChampionClass.valueOf(s))).collect(Collectors.toList());
    }

    public void setAcceptedClasses(List<String> acceptedClasses) {
        this.acceptedClasses = acceptedClasses;
    }

    public int getMaxRounds() {
        return maxRounds;
    }

    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }


    public int getMaxOrdersEachRound() {
        return maxOrdersEachRound;
    }

    public void setMaxOrdersEachRound(int maxOrdersEachRound) {
        this.maxOrdersEachRound = maxOrdersEachRound;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public int getNumberOfBots() {
        return numberOfBots;
    }

    public void setNumberOfBots(int numberOfBots) {
        this.numberOfBots = numberOfBots;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public boolean isReplyMode() {
        return replyMode;
    }

    public void setReplyMode(boolean replyMode) {
        this.replyMode = replyMode;
    }

    public boolean isRecording() {
        return recording;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
    }
}
