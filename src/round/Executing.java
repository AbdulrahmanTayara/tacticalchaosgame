package round;

import champion.Champion;
import move.BasicAttackMove;
import move.Move;
import move.WalkMove;
import player.Player;
import util.Util;


public class Executing extends Round {

    public Executing() {
        super(RoundType.EXECUTING.name());
    }

    @Override
    public void start(Player player) {
        for (Champion champion : player.getChampionsInArena()) {
            if (champion.getAttributes().isDead()) continue;
            for (int i = 0; i < champion.getMoves().size(); i++) {
                Move move = champion.getMoves().get(i);
                try {
                    move.performMove();
                } catch (Exception ignored) {}

                if (!(move instanceof BasicAttackMove) && !(move instanceof WalkMove))
                    champion.getAttributes().setMana((int) (champion.getAttributes().getMana() - champion.getAttributes().getManaCost()));
                move.setNumberOfRound(move.getNumberOfRound() - 1);
                if (move.getNumberOfRound() == 0) {
                    champion.getMoves().remove(i--);
                }
            }
            simulateChanges(champion);
        }
        player.printPlayerChampions(player.getChampionsInArena());
//        player.getPresenter().playerFinishedTheRound();
    }

    private void simulateChanges(Champion champion) {
        champion.getAttributes().setImmunityDuration(champion.getAttributes().getImmunityDuration() - 1);
        champion.getAttributes().setImmunityBasicAttackDuration(champion.getAttributes().getImmunityBasicAttackDuration() - 1);
        champion.getAttributes().setStunDuration(champion.getAttributes().getStunDuration() - 1);
    }
}