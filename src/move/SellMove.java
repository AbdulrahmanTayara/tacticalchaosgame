package move;

import champion.Champion;
import player.Player;

public class SellMove extends Move{
    Player player;
    Champion champion;
    SellMove(Player player , Champion champion){
        this.player = player;
        this.champion = champion;
    }
    @Override
    public void performMove() {
        player.sellChampion(champion);
    }
}
