package player;

import champion.Champion;
import gui.GameFrame;
import gui.IFramePresenter;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import util.ColorUtil;

import java.util.List;

public class GuiPlayer extends Player implements IFramePresenter {

    private transient IView view;
    private String color;

    public GuiPlayer() {
        super(PlayerType.GUI.name());
        view = GameFrame.getInstance();
        this.color = ColorUtil.generateColor();
    }

    public String getColor() {
        return color;
    }

    // view methods
    @Override
    public void startBuyingRound() {
        GameFrame.getInstance().setPresenter(this);
        view.startBuyingRound();
    }

    @Override
    public void startPlanningRound() {
        GameFrame.getInstance().setPresenter(this);
        view.startPlanningRound();
    }

    @Override
    public void gameFinished(Team winner) {
        view.gameFinished(winner);
    }

    @Override
    public void gameFinishedAsDraw() {
        view.gameFinishedAsDraw();
    }

    // presenter methods

    @Override
    public List<Champion> getChampionsFromStore() {
        return presenter.getChampionsFromStore();
    }

    @Override
    public void playerFinishedTheRound() {
        presenter.playerFinishedTheRound();
    }

    @Override
    public Champion buyChampionFromStore(int index) {
        return presenter.buyChampionFromStore(index);
    }

    @Override
    public void sellChampionFromStore(Champion champion) {
        presenter.sellChampionFromStore(champion);
    }

    @Override
    public List<Player> getPlayers() {
        return presenter.getPlayers();
    }

    @Override
    public int getCurrentRound() {
        return presenter.getCurrentRound();
    }

    @Override
    public List<String> getScoreBoard() {
        return presenter.getScoreBoard();
    }

    @Override
    public Player getPlayer() {
        return this;
    }

    public void pickChampionToBuy (Champion champion, int index) {
        if (canBuy(champion.getCost())) {
            buyChampion(presenter.buyChampionFromStore(index));
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No enough money", ButtonType.CLOSE);
            alert.showAndWait();
        }
    }

    public void pickChampionToSell (Champion champion) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to sell?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();
        if(alert.getResult() == ButtonType.YES) {
            sellChampion(champion);
            presenter.sellChampionFromStore(champion);
        }
    }

    @Override
    public void printPlayerChampions(List<Champion> champions) {
        System.out.println("Player No." + getId());
        for(Champion champion : champions){
            System.out.println(champion);
        }
    }

    @Override
    public void saveGame() {
        presenter.saveGame();
    }

    @Override
    public void startRecording() {
        presenter.startRecording();
    }
}
