package gui.page;

import gui.GameFrame;
import injector.AppInjector;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import player.AutoPlayer;
import player.GuiPlayer;
import player.Player;
import player.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamPickerPage {

    private Scene scene;
    private ScrollPane scrollPane;
    private List<Player> players;
    private List<Team> teams;

    public TeamPickerPage() {
        this.scrollPane = new ScrollPane();
        this.scrollPane.setPadding(new Insets(25));
        this.scene = new Scene(scrollPane);
        this.players = new ArrayList<>();
        this.teams = new ArrayList<>();
    }

    public TeamPickerPage(int numberOfPlayers, int numberOfBots) {
        this();
        int numberOfTeams = numberOfPlayers + numberOfBots;
        for (int i = 0; i < numberOfPlayers; i++) {
            players.add(new GuiPlayer());
        }
        for (int i = 0; i < numberOfBots; i++) {
            players.add(new AutoPlayer());
        }
        VBox vBox = new VBox(10);
        for (Player player : players) {
            HBox hBox = new HBox(25);
            Label label = new Label("Player " + player.getId());
            ChoiceBox<String> choiceBox = new ChoiceBox<>();
            for (int i = 0; i < numberOfTeams; i++) {
                choiceBox.getItems().add(Integer.toString(i + 1));
            }
            player.setTeamId(players.indexOf(player) + 1);
            choiceBox.setValue(Integer.toString(player.getId()));
            choiceBox.valueProperty().addListener((value, oldValue, newValue) -> player.setTeamId(Integer.parseInt(newValue)));
            hBox.getChildren().addAll(label, choiceBox);
            vBox.getChildren().add(hBox);
        }
        Button startTheGame = new Button("Start");
        startTheGame.setOnAction(e -> {
            createTeams(numberOfTeams);
            teams.forEach(team -> AppInjector.getGame().addTeam(team));
            GameFrame.getInstance().runTheGame();
        });
        vBox.getChildren().add(startTheGame);
        this.scrollPane.setContent(vBox);
    }

    private void createTeams (int numberOfTeams) {
        for (int i = 0; i < numberOfTeams; i++) {
            teams.add(new Team());
        }
        players.forEach(player -> teams.get(player.getTeamId() - 1).addPlayersToTeam(player));
        teams.removeIf(team -> team.getPlayersInTeam().isEmpty());
    }

    public Scene getScene() {
        return scene;
    }
}
