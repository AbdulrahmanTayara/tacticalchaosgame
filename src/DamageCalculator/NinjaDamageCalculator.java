package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class NinjaDamageCalculator {
    public static void Calculate(List<Champion> ninjas) {
        Attributes attributes = new Attributes();
        if(ninjas.size()==1){
            attributes.setCriticalStrikeChance(50);
            attributes.setCriticalStrikeDamage(200);
            ninjas.get(0).addToAttributes(attributes);
        }
        else if(ninjas.size()>1){
            attributes.setCriticalStrikeChance(75);
            attributes.setCriticalStrikeDamage(300);
            for(Champion champion : ninjas)
                champion.addToAttributes(attributes);
        }

    }
}
