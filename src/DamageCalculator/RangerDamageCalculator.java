package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;

public class RangerDamageCalculator {
    public static void Calculate(List<Champion> rangers) {
        Attributes attributes = new Attributes();
        if (rangers.size() >= 3)
            for (Champion champion : rangers) {
                attributes.setVisionRange(champion.getAttributes().getVisionRange());
                champion.addToAttributes(attributes);
            }
    }
}
