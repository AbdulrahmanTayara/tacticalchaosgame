package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;
import java.util.Random;

public class ActiveVolibearAbilityMove extends Move{
    Champion attacker;
    public ActiveVolibearAbilityMove(Champion attacker){
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        int abilityDamage = attacker.getAttributes().getAttackDamage() * 50/100;
        List<Champion> list = Util.getAllEnemies(attacker.getPlayerId());
        int championsToHit = 2;
        while(championsToHit > 0 && list.size() > 0){
            Attributes attributes = new Attributes();
            championsToHit--;
            Random random = new Random();
            int randomNumber = list.size() == 1 ? 0 : random.nextInt(list.size() - 1);
            Champion target = list.get(randomNumber);
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
            list.remove(randomNumber);
        }
    }
}
