package filter;

import champion.Champion;

/**
 *
 * @author Abdulrahman Kanaan
 */
public abstract class Filter {
    public abstract boolean isAccepted(Champion champion);
}
