package game;

import arena.Arena;
import champion.Champion;
import filesmanager.FileManager;
import filesmanager.IFileManger;
import injector.AppInjector;
import player.ConsolePlayer;
import player.GuiPlayer;
import player.Player;
import player.Team;
import round.*;
import store.Store;
import util.Config;
import util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Game implements IGamePresenter {


    private RoundManager roundManager;
    private ScoreBoard scoreBoard;
    private Store store;
    private Arena arena;
    private IFileManger fileManger;

    private List<Team> teams;
    private List<Round> rounds;
    private int currentRound;
    private int currentPlayer;
    private int currentTeam;
    private Config config;

    private boolean recording = false;

    public Game() {
        fileManger = new FileManager();
        rounds = new ArrayList<>();
        teams = new ArrayList<>();
        Util.setTeams(teams);
        config = AppInjector.getConfig();
        store = AppInjector.getStore();
        arena = AppInjector.getArena();
        scoreBoard = new ScoreBoard();
        currentRound = 0;
        currentPlayer = 0;
    }

    public Game(GameData gameData) {
        fileManger = new FileManager();
        rounds = gameData.getRounds();
        teams = gameData.getTeams();
        Util.setTeams(teams);
        config = AppInjector.getConfig();
        store = AppInjector.getStore();
        arena = AppInjector.getArena();
        scoreBoard = new ScoreBoard();
        currentRound = gameData.getCurrentRound();
        currentPlayer = gameData.getCurrentPlayer();
        teams.forEach(team -> team.getPlayersInTeam().forEach(player -> {
            player.setPresenter(this);
            player.getChampionsInArena().forEach(champion -> champion.setArenaObserver(arena));
            player.getChampionsInBench().forEach(champion -> champion.setArenaObserver(arena));
        }));
    }

    public void addTeam(Team team) {
        team.getPlayersInTeam().forEach(player -> {
            player.setPresenter(this);
            player.getChampionsInArena().forEach(champion -> champion.setArenaObserver(arena));
            player.getChampionsInBench().forEach(champion -> champion.setArenaObserver(arena));
        });
        teams.add(team);
    }

    @Override
    public List<Player> getPlayers() {
        List<Player> tempPlayers = new ArrayList<>();
        for (Team team : teams)
            tempPlayers.addAll(team.getPlayersInTeam());

        return tempPlayers;
    }

    public void startGame() {
        roundManager = (roundManager == null) ? new RoundManager() : roundManager;
        if (currentRound >= config.getBuyingRoundsNumber() && !arena.isInitialized()) {
            arena.setSquaresType();
        }
        Round round = rounds.get(currentRound);
        if (round instanceof Executing) {
            runExecutingRound((Executing) round);
        } else {
            round.start(teams
                    .get(currentTeam)
                    .getPlayersInTeam()
                    .get(currentPlayer));
            if (recording)
                fileManger.addRecodingState(getCurrentGameData());
        }
    }

    private void runExecutingRound(Executing round) {
        ExecutorService executors = Executors.newFixedThreadPool(getPlayers().size());
        for (Team team : teams) {
            for (Player player : team.getPlayersInTeam())
                executors.execute(() -> {
                    round.start(player);
                });
        }
        executors.shutdown();
        try {
            executors.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currentPlayer = -1;
        playerFinishedTheRound();
    }

    @Override
    public List<Champion> getChampionsFromStore() {
        store.generateChampions();
        return store.getRoundChampions();
    }

    @Override
    public void playerFinishedTheRound() {
        if (AppInjector.getConfig().isReplyMode()) return;
        checkPlayers(); // remove losers
        roundManager.runTurn();
        GameState gameState = isFinished();
        if (gameState == GameState.RUNNING) {
            startGame();
        } else { // game finished
            if (recording)
                fileManger.saveRecordingStats();
            showGameResult(gameState == GameState.DRAW);
        }
    }

    @Override
    public int getCurrentRound() {
        return currentRound;
    }

    @Override
    public List<String> getScoreBoard() {
        return scoreBoard.getEvents();
    }

    @Override
    public Champion buyChampionFromStore(int index) {
        return store.buy(index);
    }

    // remove losers
    private void checkPlayers() {
        if (currentRound < config.getBuyingRoundsNumber())
            return;
        // remove dead champions
        for (Team team : teams)
            for (Player player : team.getPlayersInTeam()) {
                player.checkChampions();
            }
        try {
            //Delete the player whose all champions in arena died
            teams.forEach(Team::checkPlayers);
            teams.removeIf(team -> {
                if (team.getPlayersInTeam().size() == 0) {
                    addToScoreBoard(team);
                    return true;
                }
                return false;
            });
        } catch (Exception e) {
        }

    }


    private GameState isFinished() {
        if (teams.size() == 1) { // finish the game
            return GameState.WINNER;
        } else if (currentRound >= config.getMaxRounds() || teams.size() == 0) { // game finished as draw
            return GameState.DRAW;
        }
        return GameState.RUNNING;
    }

    private void showGameResult(boolean isDraw) {
        if (!isDraw) {
            Team winner = teams.get(teams.size() - 1);
            addToScoreBoard(winner);
            if (config.getGameMode() == GameMode.GUI) {
                new GuiPlayer().gameFinished(winner);
            } else {
                new ConsolePlayer().gameFinished(winner);
            }
        } else {
            if (config.getGameMode() == GameMode.CONSOLE) {
                new ConsolePlayer().gameFinishedAsDraw();
            } else {
                new GuiPlayer().gameFinishedAsDraw();
            }
        }
    }

    @Override
    public void sellChampionFromStore(Champion champion) {
        store.sell(champion);
    }

    private void resetAllChampions() {
        for (Team team : teams)
            team.getPlayersInTeam().forEach(Player::resetChampions);
    }

    private void addToScoreBoard(Team team) {
        String event = "";
        if (team.getPlayersInTeam().size() == 0)
            event = "Team: " + team.getId() + " Out in round: " + currentRound + "\n";
        else
            event = "Team: " + team.getId() + " Won in round: " + currentRound + "\n";
        scoreBoard.addEvent(event);
    }


    @Override
    public void saveGame() {
        fileManger.saveGame(getCurrentGameData());
    }

    @Override
    public void startRecording() {
        if (currentRound < AppInjector.getConfig().getBuyingRoundsNumber()) return;
        if (recording) {
            fileManger.saveRecordingStats();
            System.out.println("Recording Finished");
            recording = false;
            return;
        }
        System.out.println("Recording...");
        recording = true;
        fileManger.addRecodingState(getCurrentGameData());
    }

    private GameData getCurrentGameData() {
        return new GameData(
                arena,
                teams,
                rounds,
                currentRound,
                currentPlayer,
                currentTeam,
                config
        );
    }

    class RoundManager {

        RoundManager() {
            if (rounds.isEmpty()) {
                for (int i = 0; i < config.getBuyingRoundsNumber(); i++) {
                    rounds.add(new Buying());
                }
            }
        }

        void runTurn() {
            nextPlayer();
            if (currentPlayer == 0 && currentTeam == 0) // first player
                checkRound();
        }

        private void nextPlayer() {
            currentPlayer++;
            if (currentPlayer == teams.get(currentTeam).getPlayersInTeam().size()) {
                currentPlayer = 0;
                currentTeam++;
            }

            if (currentTeam == teams.size()) {
                currentTeam = 0;
            }
        }

        private void checkRound() {
            currentRound++;
            if (currentRound == rounds.size()) {
                resetAllChampions();
                rounds.add(new Planning());
                rounds.add(new Executing());
            }
        }
    }

}
