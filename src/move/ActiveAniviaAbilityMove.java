package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveAniviaAbilityMove extends Move{

    Champion attacker;
    private static double radius = 30;
    private static int abilityDamage = 250;
    private static int stunDurationToChamps = 1;

    ActiveAniviaAbilityMove(Champion attacker){
        this.attacker = attacker;
    }
    @Override
    public void performMove() {
        List <Champion> list = Util.getEnemiesInRange(attacker.getSquare() , radius , attacker.getPlayerId());
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setStunDuration(stunDurationToChamps);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
