package player;

import arena.Square;
import champion.AbilityType;
import champion.Champion;
import injector.AppInjector;
import move.*;
import util.ColorUtil;
import util.Util;

import java.util.List;
import java.util.Random;

public class AutoPlayer extends Player {

    private String color; // for gui

    public AutoPlayer() {
        super(PlayerType.BOT.name());
        color = ColorUtil.generateColor();
    }

    @Override
    public void startBuyingRound() {
        List<Champion> storeChampions = presenter.getChampionsFromStore();
        pickChampionToBuy(storeChampions);
        fillArena();
        swapTheBestToArena();
        presenter.playerFinishedTheRound();
    }

    @Override
    public void startPlanningRound() {
        for (Champion champion : getChampionsInArena()) {
            giveBestOrderToChampion(champion);
        }
        presenter.playerFinishedTheRound();
    }

    private void fillArena() {
        while (getChampionsInArena().size() < getNumberOfChampionInArena()) {
            int benchSize = getChampionsInBench().size();
            if (benchSize == 0)
                break;
            Champion champion = getChampionsInBench().get(benchSize - 1);
            champion.setSquare(getRandomSquare());
            swapFromBenchToArena(champion);
        }
    }

    private void swapTheBestToArena() {
        for (int i = 0; i < getChampionsInBench().size(); i++) {
            for (int j = 0; j < getChampionsInArena().size(); j++) {
                Champion benchChamp = getChampionsInBench().get(i);
                Champion arenaChamp = getChampionsInArena().get(j);
                if (benchChamp.compareTo(arenaChamp) == 1) {
                    benchChamp.setSquare(getRandomSquare());
                    swapFromBenchToArena(benchChamp);
                    i--;
                    swapFromArenaToBench(arenaChamp);
                    break;
                }
            }
        }
    }

    private Square getRandomSquare() {
        Random random  = new Random();
        int arenaSize = AppInjector.getConfig().getArenaSize() - 1;
        return new Square(random.nextInt(arenaSize), random.nextInt(arenaSize));
    }

    private void giveBestOrderToChampion(Champion champion) {
        if (champion.getAttributes().getStunDuration() > 0) return;
        Champion target = null;
        int range = (champion.getAttributes().getMana() >= champion.getAttributes().getManaCost() ? champion.getAttributes().getVisionRange() : champion.getAttributes().getAttackRange());
        int min = Integer.MAX_VALUE;
        for (Champion champion1 : Util.getEnemiesInRange(champion.getSquare(), range, champion.getPlayerId())) {
            if (champion1.getAttributes().getHealth() < min) {
                min = champion1.getAttributes().getHealth();
                target = champion1;
            }
        }
        if (champion.getAttributes().getMana() >= champion.getAttributes().getManaCost() &&
                (champion.getAbilityType() == AbilityType.AOE || target != null)) {
            if (champion.getAbilityType() == AbilityType.AOE) champion.addMove(MoveFactory.getChampionAbility(champion, null));
            else champion.addMove(MoveFactory.getChampionAbility(champion, target));
        } else if (target != null) {
            champion.addMove(MoveFactory.getBasicAttack(champion, target));

        } else {
            Random random = new Random();
            int randomNumber = random.nextInt(3);
            switch (randomNumber) {
                case 0:
                    champion.addMove(new WalkMove(champion, Direction.Top));
                    break;
                case 1:
                    champion.addMove(new WalkMove(champion, Direction.Down));
                    break;
                case 2:
                    champion.addMove(new WalkMove(champion, Direction.Left));
                    break;
                case 3:
                    champion.addMove(new WalkMove(champion, Direction.Right));
                    break;
            }
        }

    }

    private void pickChampionToBuy(List<Champion> champions) {
        if (getCoins() <= 0) {
            return;
        }
        while (true) {
            Champion selectedChampion = null;
            int index = -1;
            for (int i = 0; i < champions.size(); i++) {
                Champion champion = champions.get(i);
                if (canBuy(champion.getCost())) {
                    if (isRecommended(champion) > 0) {
                        index = i;
                        break;
                    }
                    selectedChampion = champion.compareTo(selectedChampion) == 1 ? champion : selectedChampion;
                    index = i;
                }
            }
            if (index >= 0)
                buyChampion(AppInjector.getStore().buy(index));
            else
                break;
        }
    }

    @Override
    public void gameFinished(Team winner) {

    }

    @Override
    public void gameFinishedAsDraw() {
        // it is a bot
    }

    @Override
    public void printPlayerChampions(List<Champion> champions) {
        System.out.println("Player No." + getId());
        for(Champion champion : champions){
            System.out.println(champion);
        }
    }

    @Override
    public String getColor() {
        return color;
    }
}
