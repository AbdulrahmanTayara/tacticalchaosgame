package move;

import champion.Attributes;
import champion.Champion;

public class ActiveKassadinAbilityMove extends Move{
    Champion attacker , target;
    private static int abilityDamage = 20;

    public ActiveKassadinAbilityMove(Champion attacker, Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        attributes = new Attributes();
        attributes.setMana(2);
        attacker.addToAttributes(attributes);
    }
}
