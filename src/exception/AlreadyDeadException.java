package exception;

public class AlreadyDeadException extends Exception {
    public AlreadyDeadException() {
        super("Champion already dead");
    }
}
