package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;
import java.util.Random;

public class BasicAttackMove extends Move {
    Champion target , attacker;
    BasicAttackMove(Champion attacker , Champion target){
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes;
        int damage = target.getAttributes().getAttackDamage();
        int criticalStrikeChance = target.getAttributes().getCriticalStrikeChance();
        int criticalStrikeDamage = target.getAttributes().getCriticalStrikeDamage();
        Random random = new Random();
        int randomNumber = random.nextInt(100);
        if(randomNumber <= criticalStrikeChance)  damage+=(damage * criticalStrikeDamage) / 100;
        executingBasicAttack(target , damage);
        int extraChampionsToHit = attacker.getAttributes().getNumberOfExtraTergets();
        List <Champion> list = Util.getEnemiesInRange(attacker.getSquare() , attacker.getAttributes().getAttackRange() , attacker.getPlayerId());
        while(extraChampionsToHit > 0 && list.size() > 0){
            extraChampionsToHit--;
            randomNumber = random.nextInt(list.size() - 1);
            int idx = randomNumber;
            executingBasicAttack(list.get(idx) , damage);
            if(list.get(idx).getAttributes().getHealth() == 0)
                list.remove(idx);
        }
        attributes = new Attributes();
        attributes.setMana(attributes.getManaIncreasing());
        attacker.addToAttributes(attributes);
    }

    public void executingBasicAttack(Champion target , int damage){
        Random random = new Random();
        int randomNumber = random.nextInt(100);
        int numberOfTimesToHit = 1;
        if(randomNumber <= attacker.getAttributes().getHitExtraTergetschance())
            numberOfTimesToHit+=attacker.getAttributes().getNumberOfExtraAttacks();
        Attributes attributes = new Attributes();
        attributes.setAbilityType(AbilityType.basicAttack);
        attributes.setHealth(-damage * numberOfTimesToHit);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}

