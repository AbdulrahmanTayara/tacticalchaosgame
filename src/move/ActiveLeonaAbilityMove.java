package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActiveLeonaAbilityMove extends Move{

    Champion attacker;

    public ActiveLeonaAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getAllEnemies(attacker.getPlayerId());
        for(Champion  target : list){
            Attributes attributes = new Attributes();
            attributes.setStunDuration(1);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
