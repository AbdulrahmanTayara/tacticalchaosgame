package gui;

import arena.Square;
import champion.Champion;
import gui.component.GuiSquare;
import gui.page.RunningGamePage;
import gui.page.StartPage;
import injector.AppInjector;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import player.*;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GameFrame extends Application implements IView {
    private static GameFrame frame;
    private IFramePresenter presenter;
    private Stage primaryStage;
    private RunningGamePage runningGamePage;
    private static Champion movedChampion = null;
    private static GuiSquare clickedButton;
    private Timer currentTimer;
    private TimerTask currentTask;

    private boolean isPlanning;

    public static GameFrame getInstance() {
        return frame;
    }

    public void setPresenter(IFramePresenter presenter) {
        this.presenter = presenter;
    }

    public IFramePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void start(Stage stage) throws Exception {
        frame = this;
        this.primaryStage = stage;
        StartPage startPage = new StartPage(stage);
        stage.setTitle("Tactical Chaos");
        stage.setScene(startPage.getScene());
        stage.show();
    }

    public void runTheGame() {
        runningGamePage = new RunningGamePage(AppInjector.getGame().getPlayers(), AppInjector.getConfig().getTheme());
        this.primaryStage.setScene(runningGamePage.getScene());
        this.primaryStage.setMaximized(true);
        AppInjector.getGame().startGame();
    }

    public void refreshTheGame() {
        if (runningGamePage == null) {
            runTheGame();
        } else {
            runningGamePage.setPlayers(AppInjector.getGame().getPlayers());
            runningGamePage.refresh();
//            primaryStage.setMaximized(true);
//            this.primaryStage.setScene(runningGamePage.getScene());
            AppInjector.getGame().startGame();
        }
    }

    @Override
    public void startBuyingRound() {
        isPlanning = false;
        runningGamePage.buyingRound(presenter.getChampionsFromStore(), presenter.getPlayer(), presenter.getCurrentRound());
        setTimer();
    }


    private void setTimer() {
        currentTimer = new Timer(true);
        currentTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    finishTheRound();
                });
            }
        };
        currentTimer.schedule(currentTask, 100 * 1000);
    }
    public void finishTheRound() {
        currentTimer.cancel();
        currentTask.cancel();
        presenter.playerFinishedTheRound();
    }

    @Override
    public void startPlanningRound() {
        isPlanning = true;
        runningGamePage.planningRound(presenter.getChampionsFromStore(), presenter.getPlayer(), presenter.getCurrentRound());
        setTimer();
    }

    public boolean isPlanning() {
        return isPlanning;
    }

    @Override
    public void gameFinished(Team winner) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Game Finished \n The Winner is Team No." + winner.getId(), ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            showScoreBoard();
        }
    }

    @Override
    public void gameFinishedAsDraw() {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Game Finished As Draw", ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            showScoreBoard();
        }
    }

    private void showScoreBoard() {
        List<String> events = presenter.getScoreBoard();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Scoreboard: \n");
        events.forEach(event -> stringBuilder.append(event).append("\n"));
        Alert alert = new Alert(Alert.AlertType.NONE, stringBuilder.toString(), ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            System.exit(0);
        }
    }

    public void pickChampionToBuy (Champion champion, int index) {
        ((GuiPlayer)presenter.getPlayer()).pickChampionToBuy(champion, index);
        runningGamePage.refreshBench(presenter.getPlayer().getChampionsInBench());
        runningGamePage.refreshTop(presenter.getPlayer(), presenter.getCurrentRound());
        runningGamePage.refreshArena();
    }

    public void pickChampionToSell (Champion champion) {
        ((GuiPlayer)presenter.getPlayer()).pickChampionToSell(champion);
        runningGamePage.refreshBench(presenter.getPlayer().getChampionsInBench());
        runningGamePage.refreshTop(presenter.getPlayer(), presenter.getCurrentRound());
        runningGamePage.openStore();
//        clickedButton.removeChampion(champion);
        clickedButton.refreshSquare();
    }

    public void moveChampionFromBenchToStash (Champion champion) {
        if (presenter.getPlayer().getChampionsInArena().size() >= presenter.getPlayer().getNumberOfChampionInArena()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Max champions on arena reached!", ButtonType.CLOSE);
            alert.showAndWait();
        } else {
            GameFrame.setMovedChampion(champion);
            this.setCursor(Cursor.CLOSED_HAND);
            presenter.getPlayer().removeChampionFromBench(champion);
        }
    }

    public void moveChampionFromStashToArena () {
        Square square = new Square(clickedButton.getSquare().getX() , clickedButton.getSquare().getY());
        GameFrame.getMovedChampion().setSquare(square);
        this.getPresenter().getPlayer().addChampionToArena(GameFrame.getMovedChampion());
        GameFrame.setMovedChampion(null);
        this.setCursor(Cursor.DEFAULT);
        clickedButton.refreshSquare();
    }

    public void moveChampionFromArenaToBench (Champion champion) {
//        clickedButton.removeChampion(champion);
        presenter.getPlayer().removeChampionFromArena(champion);
        champion.setSquare(new Square(-1 , -1));
        presenter.getPlayer().addChampionToBench(champion);
        clickedButton.refreshSquare();
        runningGamePage.refreshBench(presenter.getPlayer().getChampionsInBench());
        runningGamePage.openStore();
    }

    public void setCursor (Cursor cursor) {
        this.primaryStage.getScene().setCursor(cursor);
    }

    public RunningGamePage getRunningGamePage() {
        return runningGamePage;
    }

    public static GuiSquare getClickedButton() {
        return clickedButton;
    }

    public static void setClickedButton(GuiSquare clickedButton) {
        GameFrame.clickedButton = clickedButton;
    }

    public static Champion getMovedChampion() {
        return movedChampion;
    }

    public static void setMovedChampion(Champion movedChampion) {
        GameFrame.movedChampion = movedChampion;
    }


}
