package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;

public class ActivePykeAbilityMove extends Move{
    private static int abilityDamage = 100;
    Champion attacker;

    public ActivePykeAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        List<Champion> list = Util.getEnemiesInALine(attacker.getSquare() , attacker.getPlayerId() , 50);
        for(Champion target : list){
            Attributes attributes = new Attributes();
            attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
        }
    }
}
