package gui.view;

import champion.AbilityType;
import champion.Champion;
import gui.GameFrame;
import gui.component.DetailsTooltip;
import injector.AppInjector;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import move.Direction;
import move.Move;
import move.MoveFactory;
import move.WalkMove;
import util.Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class ChampionView {

    private Champion champion;
    private HBox hBox;
    private boolean canControl;
    private boolean isPlanning;

    public ChampionView() {
        this.hBox = new HBox(10);
        this.hBox.getStyleClass().add("theme");
    }

    public ChampionView (Champion champion, boolean isPlanning) {
        this();
        this.champion = champion;
        this.isPlanning = isPlanning;
        this.canControl = this.champion.getPlayerId() == GameFrame.getInstance().getPresenter().getPlayer().getId();
        this.prepareChampionView();
    }

    public void prepareChampionView () {
        this.hBox.getChildren().clear();
        Image image = null;
        try {
            image = new Image(new FileInputStream("Data/Img/Champions/" + champion.getName() + ".png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.hBox.setPadding(new Insets(25,150,25,150));

//        Champion Profile
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(100);
        imageView.setFitWidth(100);
        Label label = new Label(this.champion.getName());
        label.setAlignment(Pos.CENTER);
        label.setStyle("-fx-font-weight: bolder; -fx-font-size: 20px");
        label.setMaxWidth(Double.MAX_VALUE);
        VBox championProfile = new VBox();
        championProfile.getChildren().addAll(label, imageView);

//        Champion Data
        VBox championData = new VBox();
        championData.setPadding(new Insets(25));
        Label hitPoints = new Label("HP: " + this.champion.getAttributes().getHealth() + "/" + this.champion.getAttributes().getHealthCapacity());
        hitPoints.setStyle("-fx-font-size: 16px");
        Label mana = new Label("Mana: " + this.champion.getAttributes().getMana());
        mana.setStyle("-fx-font-size: 16px");
        Label level = new Label("Level: " + this.champion.getAttributes().getLevel());
        level.setStyle("-fx-font-size: 16px");
        championData.getChildren().addAll(hitPoints, mana, level);

//        Champion Control
        HBox championControl = new HBox(10);
        if (this.canControl) {
            championControl.setAlignment(Pos.CENTER);
            Button moveToBench = new Button("Move To Bench");
            moveToBench.setOnAction(e -> {
                GameFrame.getInstance().moveChampionFromArenaToBench(champion);
            });

            Button sell = new Button("Sell");
            sell.setOnAction(e -> {
                GameFrame.getInstance().pickChampionToSell(champion);
            });

            Button details = new Button("Details");
            DetailsTooltip detailsTooltip = new DetailsTooltip(champion);
            details.setOnMousePressed(actionEvent -> {
                detailsTooltip.toggleTooltip(details, actionEvent.getScreenX(), actionEvent.getScreenY());
            });

            championControl.getChildren().addAll(moveToBench, sell, details);
        }

//        Champion Commands
        HBox championCommands = new HBox(10);
        if (this.isPlanning && this.canControl) {
            // TODO:
            //  check if: champion.getMoves().size() >= AppInjector.getConfig().getMaxOrdersEachRound()
            //  get all targets that champ can reach and add some basic commands
            if (champion.getAttributes().getStunDuration() > 0) {
                this.hBox.getChildren().add(new Label("Stunned, Can't receive order."));
            } else {

                Button walkingButton = new Button("Walk");
                walkingButton.setOnAction(e -> {
                    Stage stage = new Stage();
                    stage.setTitle(walkingButton.getText());
                    VBox vBox = new VBox(10);
                    vBox.setAlignment(Pos.CENTER);
                    vBox.setPadding(new Insets(10));

                    Button left = new Button("Left");
                    left.setOnAction(t -> this.addMove(new WalkMove(champion, Direction.Left), stage));

                    Button right = new Button("Right");
                    right.setOnAction(t -> this.addMove(new WalkMove(champion, Direction.Right), stage));

                    Button top = new Button("Top");
                    top.setOnAction(t -> this.addMove(new WalkMove(champion, Direction.Top), stage));

                    Button down = new Button("Down");
                    down.setOnAction(t -> this.addMove(new WalkMove(champion, Direction.Down), stage));

                    vBox.getChildren().addAll(left, right, top, down);
                    stage.setScene(new Scene(vBox));
                    stage.show();
                });

                Button attackButton = new Button("Attack");
                attackButton.setOnAction(e -> {
                    List<Champion> targets = Util.getEnemiesInRange(champion.getSquare(), champion.getAttributes().getAttackRange(), champion.getPlayerId());
                    if (targets.size() == 0) {
                        Alert alert = new Alert(Alert.AlertType.WARNING, "No enemies in attack range!", ButtonType.CLOSE);
                        alert.showAndWait();
                    } else {
                        Stage stage = new Stage();
                        stage.setTitle(attackButton.getText());
                        ScrollPane scrollPane = new ScrollPane();
                        VBox vBox = new VBox(10);
                        vBox.setAlignment(Pos.CENTER);
                        vBox.setPadding(new Insets(10));
                        for (Champion target:targets) {
                            Button targetButton = new Button(target.getName());
                            targetButton.setOnAction(t -> {
                                this.addMove(MoveFactory.getBasicAttack(this.champion, target), stage);
                            });
                            vBox.getChildren().add(targetButton);
                        }
                        scrollPane.setContent(vBox);
                        stage.setScene(new Scene(scrollPane));
                        stage.show();
                    }
                });

                Button abilityButton = new Button("Ability");
                abilityButton.setOnAction(e -> {
                    List<Champion> targets = Util.getEnemiesInRange(champion.getSquare(), champion.getAttributes().getVisionRange(), champion.getPlayerId());
                    if (this.champion.getAttributes().getMana() >= champion.getAttributes().getManaCost()) {
                        Stage stage = new Stage();
                        stage.setTitle(abilityButton.getText());
                        ScrollPane scrollPane = new ScrollPane();
                        VBox vBox = new VBox(10);
                        vBox.setAlignment(Pos.CENTER);
                        vBox.setPadding(new Insets(10));
                        if (this.champion.getAbilityType() == AbilityType.AOE) {
                            Button tempButton = new Button("Use AOE ability");
                            tempButton.setOnAction(t -> this.addMove(MoveFactory.getChampionAbility(this.champion, null), stage));
                            vBox.getChildren().add(tempButton);
                        } else {
                            for (Champion target:targets) {
                                Button targetButton = new Button(target.getName());
                                targetButton.setOnAction(t -> this.addMove(MoveFactory.getChampionAbility(this.champion, target), stage));
                                vBox.getChildren().add(targetButton);
                            }
                        }
                        scrollPane.setContent(vBox);
                        stage.setScene(new Scene(scrollPane));
                        stage.show();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "No enough mana!", ButtonType.CLOSE);
                        alert.showAndWait();
                    }
                });

                Button viewCommandsButton = new Button("Commands");
                viewCommandsButton.setOnAction(e -> {
                    Stage stage = new Stage();
                    stage.setTitle(viewCommandsButton.getText());
                    VBox commandsVBox = new VBox(10);
                    commandsVBox.setPadding(new Insets(10));

                    int i = 1;
                    for(Move move : this.champion.getMoves()) {
                        commandsVBox.getChildren().add(new Label(i + ") " + move.toString()));
                        i++;
                    }

                    stage.setScene(new Scene(commandsVBox));
                    stage.show();

                });

                championCommands.getChildren().addAll(walkingButton, attackButton, abilityButton, viewCommandsButton);
            }
        }

        VBox controlsVBox = new VBox(10);
        controlsVBox.getChildren().addAll(championControl, championCommands);


        this.hBox.getChildren().addAll(championProfile, championData, controlsVBox);
    }

    public Node getChampionView () {
        return this.hBox;
    }

    public void addMove (Move move, Stage stage) {
        stage.close();
        if (champion.getMoves().size() >= AppInjector.getConfig().getMaxOrdersEachRound()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Max order per round reached!", ButtonType.CLOSE);
            alert.showAndWait();
        } else {
            this.champion.addMove(move);
        }
    }

}
