package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;
import util.Util;

import java.util.List;
import java.util.Random;

public class ActiveKayleAbilityMove extends Move{
    Champion attacker;

    public ActiveKayleAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        attributes.setImmunityDuration(1);
        attacker.acceptDamage(attributes);
        List<Champion> list = Util.getAllAllies(attacker.getPlayerId());
        list.remove(attacker);
        int championsToProtect = 2;
        while(championsToProtect > 0 && list.size() > 0){
            attributes = new Attributes();
            championsToProtect--;
            Random random = new Random();
            int randomNumber = random.nextInt(list.size() - 1);
            Champion target = list.get(randomNumber);
            attributes.setImmunityDuration(1);
            attributes.setAbilityType(AbilityType.ability);
            attributes.merge(attacker.getAttackAttributes());
            target.acceptDamage(attributes);
            list.remove(randomNumber);
        }
    }
}
