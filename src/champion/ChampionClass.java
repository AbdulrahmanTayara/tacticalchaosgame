package champion;

public enum ChampionClass {
    Demon("Demon"),
    Dragon("Dragon"),
    Glacial("Glacial"),
    Imperial("Imperial"),
    Noble("Noble"),
    Ninja("Ninja"),
    Pirate("Pirate"),
    Wild("Wild"),
    Void("Void"),
    Yordle("Yordle"),
    Assassin("Assassin"),
    BladeMaster("BladeMaster"),
    Brawler("Brawler"),
    Elementalist("Elementalist"),
    Gunslinger("Gunslinger"),
    Knight("Knight"),
    Ranger("Ranger"),
    Shapeshifter("Shapeshifter"),
    Sorcerer("Sorcerer");

    private String value;

    ChampionClass(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
