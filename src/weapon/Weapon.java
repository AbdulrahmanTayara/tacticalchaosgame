package weapon;

import champion.Attributes;
import champion.Champion;
import champion.ChampionClass;
import com.google.gson.annotations.SerializedName;

import java.util.Random;

public abstract class Weapon {

    protected ChampionClass championClass;

    private String name;

    @SerializedName("weaponType")
    private String weaponType;

    public Weapon() {
    }

    public Weapon(String weaponType, String name) {
        this.weaponType = weaponType;
        this.name = name;
    }

    public ChampionClass getChampionClass() {
        return championClass;
    }

    public void setChampionClass(ChampionClass championClass) {
        this.championClass = championClass;
    }


    public String getName() {
        return name;
    }

    public static Weapon getRandomWeapon() {
        Random random = new Random();
        int randomNumber = random.nextInt(6);
        switch (randomNumber){
            case 0:
                return new AngryCloak();
            case 1:
                return new KnightArmor();
            case 2:
                return new MagicHat();
            case 3:
                return new NightShift();
            case 4:
                return new UniverseCore();
            case 5:
                return new VoidHit();
            case 6:
                return new WarriorGloves();
        }
        return null;
    }

    public abstract Attributes getWeaponBuff();
}
