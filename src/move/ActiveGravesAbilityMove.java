package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveGravesAbilityMove extends Move{
    private static int abilityDamage = 300;
    Champion attacker , target;

    public ActiveGravesAbilityMove(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        int targetCriticalChance = target.getAttributes().getCriticalStrikeChance();
        Attributes attributes = new Attributes();
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setCriticalStrikeChance(-targetCriticalChance);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
