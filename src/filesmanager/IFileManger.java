package filesmanager;

import game.GameData;

import java.util.List;

public interface IFileManger {
    void saveGame(GameData gameData);
    GameData loadGame(String path);
    void addRecodingState(GameData gameData);
    void saveRecordingStats();
    List<GameData> getRecordingStates();
}