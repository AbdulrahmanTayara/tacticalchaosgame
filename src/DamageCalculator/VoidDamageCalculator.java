package DamageCalculator;

import champion.Attributes;
import champion.Champion;

import java.util.List;
import java.util.Random;

public class VoidDamageCalculator {
    public static void Calculate(List<Champion> voids) {
        Attributes attributes = new Attributes();
        attributes.setTrueDamage(true);
        Random random = new Random();
        if (voids.size() >= 2 && voids.size() < 4)
            voids.get(random.nextInt(voids.size())).addToAttributes(attributes);
        else if (voids.size() >= 4)
            for (Champion champion : voids)
                champion.getAttributes().setTrueDamage(true);

    }
}
