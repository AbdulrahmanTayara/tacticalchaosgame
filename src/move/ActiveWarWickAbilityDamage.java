package move;

import champion.AbilityType;
import champion.Attributes;
import champion.Champion;

public class ActiveWarWickAbilityDamage extends Move{
    Champion target,attacker;

    public ActiveWarWickAbilityDamage(Champion attacker , Champion target) {
        this.attacker = attacker;
        this.target = target;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int targetMaxHealth = target.getAttributes().getHealthCapacity();
        int abilityDamage = (targetMaxHealth * 10/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
        attributes = new Attributes();
        attributes.setHealth((targetMaxHealth * 10/100));
        attacker.addToAttributes(attributes);
    }
}
