package move;

import champion.Attributes;
import champion.Champion;
import util.Util;

public class ActiveNidaleAbilityMove extends Move{
    Champion attacker;
    private static int healing = 100;

    public ActiveNidaleAbilityMove(Champion attacker) {
        this.attacker = attacker;
    }

    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int attackDamage = attacker.getAttributes().getAttackDamage();
        attributes.setAttackRange(attackDamage * 10/100);
        attacker.addToAttributes(attributes);
        Champion allie = Util.getNearestAllie(attacker.getSquare() , attacker.getPlayerId() , attacker.getId());
        attributes = new Attributes();
        attributes.setHealth(100);
        allie.addToAttributes(attributes);
    }
}
