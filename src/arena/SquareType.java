package arena;

public enum SquareType {
    Grass,
    Terrain,
    Water,
    Standard
}
