import game.Game;
import gui.GameFrame;
import javafx.application.Application;
import player.AutoPlayer;
import player.ConsolePlayer;
import player.Player;
import util.InputUtil;

public class Main {


    public static void main(String[] args) {
        int gameMode;
        System.out.println("Enter game mode : 1) GUI \t 2) Console ");
        gameMode = InputUtil.getInt();

        if (gameMode == 1) { // gui
            initializeGuiMode();
        } else { // console
            initializeConsoleMode();
        }
    }

    private static void initializeGuiMode() {
        Application.launch(GameFrame.class);
    }

    private static void initializeConsoleMode() {
//        Game game = AppInjector.getGame();
//        System.out.println("This mode will use default values ^-^ ");
//        while (true) {
//            System.out.println("Do you want to add new player ? 1) Yes \t 2) No");
//            int addPlayer = InputUtil.getInt();
//            if (addPlayer == 1) {
//                game.addPlayer(choosePlayer());
//            } else {
//                break;
//            }
//        }
//        game.startGame();
    }

    private static Player choosePlayer() {
        System.out.println("1) BOT \t 2) Normal");
        int type = InputUtil.getInt();
        return type == 1 ? new AutoPlayer() : new ConsolePlayer();
    }
}

