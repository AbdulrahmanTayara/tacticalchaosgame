package move;

import champion.Attributes;
import champion.Champion;
import champion.AbilityType;

public class ActiveAkaliAbilityMove extends Move{

    Champion attacker;
    Champion target;
    ActiveAkaliAbilityMove(Champion attacker , Champion target){
        this.attacker = attacker;
        this.target = target;
    }
    @Override
    public void performMove() {
        Attributes attributes = new Attributes();
        int healthCapacity = target.getAttributes().getHealthCapacity();
        int abilityDamage = (healthCapacity * 10/100);
        attributes.setHealth(-abilityDamage - (attacker.getAttributes().getAbilityPower() * abilityDamage)/100);
        attributes.setAbilityType(AbilityType.ability);
        attributes.merge(attacker.getAttackAttributes());
        target.acceptDamage(attributes);
    }
}
