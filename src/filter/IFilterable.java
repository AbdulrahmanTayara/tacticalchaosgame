
package filter;

public interface IFilterable {
    public void addFilter(Filter filterObject);
}
