package gui.page;

import champion.Champion;
import gui.*;
import gui.view.ArenaView;
import gui.view.BenchView;
import gui.view.ChampionView;
import gui.view.StoreView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import player.Player;

import java.util.List;

public class RunningGamePage {
    private Scene scene;
    private BorderPane borderPane;
    private List<Player> players;
    private StoreView storeView;
    private boolean isPlanning;
    private ArenaView arenaView;
    private String theme;

    public RunningGamePage(List<Player> players, String theme) {
        this.theme = theme;
        this.players = players;
        this.prepareBorderPane();
        this.scene = new Scene(this.borderPane, 640, 480);
        this.scene.getStylesheets().add(getClass().getResource("/Styles/" + theme.toLowerCase() + "theme.css").toExternalForm());
        this.isPlanning = false;
    }

    private void prepareScene() {
        this.prepareBorderPane();
//        this.scene = new Scene(this.borderPane, 640, 480);
//        this.scene.getStylesheets().add(getClass().getResource("/Styles/" + theme.toLowerCase() + "theme.css").toExternalForm());
    }

    private void prepareBorderPane() {
        if (borderPane == null)
            borderPane = new BorderPane();
        //this.borderPane = new BorderPane();
        this.borderPane.setCenter(null);
        this.borderPane.setTop(null);
        this.borderPane.setLeft(null);
        this.borderPane.setRight(null);
        this.borderPane.setBottom(null);

        this.borderPane.setCenter(this.prepareCenter());
        this.borderPane.setTop(this.prepareTop());
        this.borderPane.setLeft(this.prepareLeft());
        this.borderPane.setRight(this.prepareRight());
        this.borderPane.setBottom(this.prepareBottom());
    }

    private Node prepareCenter() {
        this.arenaView = new ArenaView();
        return this.arenaView.getArenaView();
    }

    private Node prepareTop() {
        return null;
    }

    private Node prepareRight() {
        return null;
    }

    private Node prepareLeft() {
        VBox playersList = new VBox(10);
        playersList.getStyleClass().add("theme");
        playersList.setPadding(new Insets(10));
        playersList.setAlignment(Pos.TOP_CENTER);
        for (Player player : players) {
            Label myLabel = new Label("Player" + player.getId());
            myLabel.setStyle("-fx-text-fill: white; -fx-background-color: #" + player.getColor() + ";");
            myLabel.setPadding(new Insets(8, 25, 8, 25));
            playersList.getChildren().add(myLabel);
            playersList.getChildren().add(new Separator());
        }
        Button button = new Button("End turn");
        button.setOnAction(e -> {
            GameFrame.getInstance().finishTheRound();
        });

        Button saveButton = new Button("Save");
        saveButton.setOnAction(e -> {
            GameFrame.getInstance().getPresenter().saveGame();
        });

        Button recordButton = new Button("Record");
        recordButton.setOnAction(e -> {
            GameFrame.getInstance().getPresenter().startRecording();
        });

        playersList.getChildren().addAll(button, saveButton, recordButton);
        playersList.setStyle("-fx-font-size: 18px;");

        return playersList;
    }

    private Node prepareBottom() {
        return null;
    }


    public void buyingRound(List<Champion> storeChampions, Player currentPlayer, int currentRound) {
        this.arenaView.refreshArenaView();
        this.storeView = new StoreView(storeChampions);
        this.borderPane.setBottom(storeView.getStoreView());
        BenchView benchView = new BenchView(currentPlayer.getChampionsInBench());
        this.borderPane.setRight(benchView.getBenchView());
        this.refreshTop(currentPlayer, currentRound);
    }

    public void planningRound(List<Champion> storeChampions, Player currentPlayer, int currentRound) {
        this.arenaView.refreshArenaView();
        this.isPlanning = true;
        this.storeView = new StoreView(storeChampions);
        this.borderPane.setBottom(storeView.getStoreView());
        BenchView benchView = new BenchView(currentPlayer.getChampionsInBench());
        this.borderPane.setRight(benchView.getBenchView());
        this.refreshTop(currentPlayer, currentRound);
    }

    public void refreshTop (Player currentPlayer, int currentRound) {
        Button storeButton = new Button("Store");
        storeButton.setOnAction(e -> {
            this.openStore();
        });
        HBox topHBox = new HBox(10);
        topHBox.setAlignment(Pos.CENTER);
        topHBox.setPadding(new Insets(10));
        topHBox.getChildren().addAll(
                new Label("Round: " + (currentRound + 1)),
                new Label("|"),
                new Label("Player " + currentPlayer.getId()),
                new Label("|"),
                new Label("Coins: " + currentPlayer.getCoins()),
                new Label("|"),
                storeButton
        );
        topHBox.getStyleClass().add("theme");
        this.borderPane.setTop(topHBox);
    }

    public void refreshBench (List<Champion> benchChampions) {
        BenchView benchView = new BenchView(benchChampions);
        this.borderPane.setRight(benchView.getBenchView());
    }

    public Scene getScene() {
        return scene;
    }

    public void openChampionView (Champion champion) {
        this.borderPane.setBottom(new ChampionView(champion, isPlanning).getChampionView());
    }

    public void openStore () {
        this.borderPane.setBottom(storeView.getStoreView());
    }

    public void refreshArena () {
        this.arenaView.refreshArenaView();
    }

    public void refresh() {
        prepareScene();
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }


}
